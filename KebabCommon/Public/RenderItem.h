#pragma once
#include "PublicDef.h"
#include "Matrix.h"


// TODO: Handle state changes to render items outside -> make it a POD 
struct KEBABCOMMON_API RenderItem
{
	virtual ~RenderItem() = default;
	
	RenderItem(RenderItem&&);
	virtual RenderItem& operator=(RenderItem&&);
	
	RenderItem(const RenderItem&) = delete;
	RenderItem& operator=(const RenderItem&) = delete;

	Matrix transform;
	ResourceHandle mesh;

protected:
	RenderItem();
};