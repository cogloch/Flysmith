#pragma once
#include <array>
#include <vector>
#include "Vector.h"
#include "Matrix.h"


class KEBABCOMMON_API CubicBezierCurve
{
public:
	CubicBezierCurve();

	void SetControlPoints(const std::array<Vector2, 4>& controlPoints);
	
	// Assume valid control points have been set. 
	// The size of the passed in array corresponds to the sample size. 
	void GetSamples(std::vector<Vector2>* pOutSamples);
	
	// Assume valid control points have been set. 
	// t is in the [0..1] range 
	Vector2 GetSample(float t);

	// Used mainly by the bicubic surface gen -- avoid having the constant mat in 2 places.
	static const Matrix GetBasis() { return s_basisMat; }

private:
	bool m_bSetControlPoints;
	Vector2 m_controlPoints[4];

	// Cached after setting valid control points. 
	// gm = G * M                                                      3x4
	// G = geometry matrix dependent on control points                 3x4
	// M = (constant) basis matrix specific to cubic bezier curves     4x4
	Matrix3x4 m_gmMatrix;

	static const Matrix s_basisMat;
};