#pragma once


class IResource
{
public:
	// Returns false on failure.
	virtual bool LoadFromFile(const char*) = 0;
};