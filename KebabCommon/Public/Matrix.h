#pragma once
#include "PublicDef.h"


// 4x4 Matrix
struct KEBABCOMMON_API Matrix
{
	struct Vector { float x, y, z, w; };

	union
	{
		// MSVC complains anonymous structs even though they are in the C11 standard
		#pragma warning(push)
		#pragma warning(disable:4201)
		struct
		{
			float m11, m12, m13, m14;
			float m21, m22, m23, m24;
			float m31, m32, m33, m34;
			float m41, m42, m43, m44;
		};
		#pragma warning(pop)

		float m[4][4];
		Vector M[4];
	};

	DirectX::XMMATRIX GetXMMatrix() const;
	void SetFromXM(DirectX::CXMMATRIX);

	Matrix operator*(const Matrix&) const;
	Matrix Transpose() const;

	Matrix();
	Matrix(float m11, float m12, float m13, float m14,
		   float m21, float m22, float m23, float m24,
		   float m31, float m32, float m33, float m34,
		   float m41, float m42, float m43, float m44);

	// Returns the element at [row][col]
	float operator()(size_t row, size_t col) const { return m[row][col]; }
};

// 3x3 Matrix
struct KEBABCOMMON_API Matrix3
{
	struct Vector { float x, y, z; };

	union
	{
		// MSVC complains anonymous structs even though they are in the C11 standard
		#pragma warning(push)
		#pragma warning(disable:4201)
		struct
		{
			float m11, m12, m13;
			float m21, m22, m23;
			float m31, m32, m33;
		};
		#pragma warning(pop)

		float m[3][3];
		Vector M[3];
	};

	DirectX::XMMATRIX GetXMMatrix() const;
	void SetFromXM(DirectX::CXMMATRIX);

	Matrix3();
};

// 3x4 Matrix
struct KEBABCOMMON_API Matrix3x4
{
	union
	{
		// MSVC complains anonymous structs even though they are in the C11 standard
		#pragma warning(push)
		#pragma warning(disable:4201)
		struct
		{
			float m11, m12, m13, m14;
			float m21, m22, m23, m24;
			float m31, m32, m33, m34;
		};
		#pragma warning(pop)

		float m[3][4];
	};

	Matrix3x4();
	Matrix3x4(float m11, float m12, float m13, float m14,
			  float m21, float m22, float m23, float m24,
		      float m31, float m32, float m33, float m34);

	float operator()(size_t row, size_t col) const { return m[row][col]; }
};

Matrix3x4 KEBABCOMMON_API operator*(const Matrix3x4&, const Matrix&);