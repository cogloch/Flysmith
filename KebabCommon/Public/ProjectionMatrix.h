#pragma once
#include "PublicDef.h"
#include "Matrix.h"


class KEBABCOMMON_API ProjectionMatrix
{
public:
	Matrix GetMatrix() const { return m_projMatrix; }
	
protected:
	Matrix m_projMatrix;
	ProjectionMatrix() = default;
};