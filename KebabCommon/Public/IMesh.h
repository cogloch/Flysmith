#pragma once
#include "PublicDef.h"


// Separate renderers will inherit from Mesh to add API specific resource handles needed for rendering
class KEBABCOMMON_API IMesh
{
public:
	virtual ~IMesh() = default;

	// Derived classes should implement move constrs since they will likely contain resource handles
	IMesh(IMesh&& other) = default;
	virtual IMesh& operator=(IMesh&& other) = default;

	// Non-copyable since meshes contain resource handles
	IMesh(const IMesh&) = delete;
	IMesh& operator=(const IMesh&) = delete;

	virtual U32 GetNumIndices() const = 0;

protected:
	IMesh() = default;
};