#pragma once
#include "PublicDef.h"
#include "Matrix.h"


// Used to copy render state from the simulation system to the renderer. 
struct KEBABCOMMON_API RenderItemProxy
{
	RenderItemProxy(const Matrix& transform, const U32 renderItemId);
	const Matrix transform;
	const U32 renderItemId;
};