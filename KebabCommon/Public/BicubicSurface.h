#pragma once
#include "Vector.h"
#include "Matrix.h"
#include <vector>
#include <array>


enum class CubicCurveClass
{
	BEZIER
};

class KEBABCOMMON_API BicubicSurfacePatch
{
public:
	BicubicSurfacePatch(CubicCurveClass = CubicCurveClass::BEZIER);
	void SetClass(CubicCurveClass);

	void SetControlPoints(const std::array<Vector3, 16>&);

	// Assumes valid control points have been set. 
	// Outputs numSamplesPerAxis*numSamplesPerAxis points.
	std::vector<Vector3> GetSamples(U32 numSamplesPerAxis);
	
	// Assumes valid control points have been set. 
	// s and t are in the [0..1] range 
	Vector3 GetSample(float s, float t);

private:
	bool m_bSetControlPoints;
	Vector3 m_controlPoints[16];
	CubicCurveClass m_class;

	void ComputeIndependentMat();
	// Product of basis transposed, geometrical constraint matrix and basis,
	// hence dependent only on control points and the class of curve used
	// hence independent of parametrization.
	// One for each axis.
	Matrix m_independentMat[3];
};