#pragma once
#include "PublicDef.h"


struct KEBABCOMMON_API Vector2
{
	union
	{
		// MSVC complains anonymous structs even though they are in the C11 standard
		#pragma warning(push)
		#pragma warning(disable:4201)
		struct
		{
			float x;
			float y;
		};
		#pragma warning(pop)

		float val[2];
	};
	
	// Set all components to 0
	Vector2();

	// Set the x and y components to the passed @x and @y
	Vector2(float x, float y);
	explicit Vector2(I32 x, I32 y);
	explicit Vector2(U32 x, U32 y);

	// Set all components to @uniformValue
	Vector2(float uniformValue);

	Vector2 operator+=(const Vector2&);
	Vector2 operator*=(float scale);

	DirectX::XMVECTOR GetXMVec() const;
	void Set(DirectX::CXMVECTOR);
};

struct KEBABCOMMON_API Vector3
{
	union
	{
		// MSVC complains anonymous structs even though they are in the C11 standard
		#pragma warning(push)
		#pragma warning(disable:4201)
		struct
		{
			float x;
			float y;
			float z;
		};
		#pragma warning(pop)

		float val[3];
	};

	// Set all components to 0
	Vector3();

	// Set the x, y and z components to the passed @x, @y and @z
	Vector3(float x, float y, float z);

	// Set all components to @uniformValue
	Vector3(float uniformValue);

	// Set x and y to @vec's x and y and the z component to @z
	Vector3(Vector2 vec, float z = 0.0f);

	Vector3& operator=(const Vector3&);
	Vector3 operator+(const Vector3&) const;
	Vector3 operator-(const Vector3&) const;
	Vector3 operator*(float scale) const;
	Vector3 operator/(float scale) const;
	Vector3 operator+=(const Vector3&);
	Vector3 operator-=(const Vector3&);
	Vector3 operator*=(float scale);
	Vector3 operator/=(float scale);

	float GetMagnitude() const;
	float GetMagnitudeSq() const ;
	void Normalize();

	DirectX::XMVECTOR GetXMVec() const;
	void Set(DirectX::CXMVECTOR);
};

Vector2 KEBABCOMMON_API operator*(float scale, const Vector2&);
Vector3 KEBABCOMMON_API operator*(float scale, const Vector3&);
bool KEBABCOMMON_API operator!=(const Vector2&, const Vector2&);
bool KEBABCOMMON_API operator!=(const Vector3&, const Vector3&);
bool KEBABCOMMON_API operator==(const Vector2&, const Vector2&);
bool KEBABCOMMON_API operator==(const Vector3&, const Vector3&);