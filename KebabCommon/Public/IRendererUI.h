#pragma once
#include "PublicDef.h"


using BatchId = U32;

// Dummy interface for now, since focus is on ogl side. 
class KEBABCOMMON_API IRendererUI
{
public:
	virtual ~IRendererUI() = default;
protected:
	IRendererUI() = default;
};