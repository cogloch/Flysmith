#pragma once
#include <map>
#include "PublicDef.h"
#include "Vector.h"
#include "IResource.h"


struct FT_Bitmap_;

struct KEBABCOMMON_API GlyphGeneric
{
	Vector2 size         = {0, 0};
	Vector2 bearing      = {0, 0};
	Vector2 advance      = {0, 0};
	unsigned char* data  = nullptr;
};

struct KEBABCOMMON_API Font : public IResource
{
	~Font();
	bool LoadFromFile(const char* path) override;
	GlyphGeneric glyphs[128];

	struct KEBABCOMMON_API FontAtlas
	{
		U32 width  = 0;  // Sum of glyph widths.
		U32 height = 0;  // Max glyph height.
	} atlas;
};