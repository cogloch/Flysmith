#pragma once
#include "PublicDef.h"

#include "TransformNoScale.h"
#include "RenderItemProxy.h"
#include "ShaderTypes.h"
#include "Vertex.h"
#include <vector>


using RenderItemHandle = U32;

class KEBABCOMMON_API IRenderer
{
public:
	// Derived classes are responsible for destroying their impl instances. 
	virtual ~IRenderer() = default;

	RenderItemHandle AddRenderItem(ResourceHandle mesh, ResourceHandle vertexShader, ResourceHandle pixelShader);

	// Populate a list of render item proxies from render component data and update 
	// the render state for the next frame.
	virtual void UpdateScene(std::vector<RenderItemProxy>) = 0;

	// Copy camera transform
	virtual void UpdateView(const TransformNoScale& camTransform) const = 0;

	virtual void Render() const = 0;

	// TODO: Queue resource caching requests for later 
	virtual ResourceHandle CacheMesh(const VertexVec& verts, const IndexVec& indices) const = 0;
	virtual void UpdateMesh(ResourceHandle, const VertexVec& verts, const IndexVec& indices) const = 0;
	virtual ResourceHandle CacheShader(ShaderType, const std::wstring& fullPath) const = 0;

protected:
	IRenderer();

	struct Impl;
	Impl* m_pImpl;

protected:
	static const size_t MAX_RENDER_QUEUE_ITEMS = 100;
	static const size_t MAX_RENDER_ITEMS = 100;

	// Holds ids of render items that are going to be rendered this frame.
	U32 m_renderQueue[MAX_RENDER_QUEUE_ITEMS];
	size_t m_renderQueueEnd;

	struct RenderItemCacheRequest
	{
		ResourceHandle mesh;
		ResourceHandle vertShader;
		ResourceHandle pixelShader;
	};
	RenderItemCacheRequest m_renderItemCacheQueue[MAX_RENDER_ITEMS];
	size_t m_numRenderItemCacheRequests;

	size_t m_numRenderItems;
};