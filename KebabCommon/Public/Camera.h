#pragma once
#include "TransformNoScale.h"
#include "ProjectionMatrix.h"
#include "Matrix.h"


class KEBABCOMMON_API Camera
{
public:
	Camera();
	~Camera() = default;

	const Matrix& GetViewMatrix();
	const Matrix& GetViewProjMatrix(const ProjectionMatrix&);
	
	// Update the view matrix, given a new transform for the camera.
	void Update(const TransformNoScale& newTransform);
	const TransformNoScale& GetTransform() const { return m_transform; }

private:
	void CacheViewMatrix();

	bool m_bChanged;
	TransformNoScale m_transform;
	
	Matrix m_viewMatrix;
	Matrix m_viewProjMatrix;
};