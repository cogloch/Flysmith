#include "OrthographicMatrix.h"


OrthographicMatrix::OrthographicMatrix(float width, float height, float nearPlane, float farPlane)
{
	m_projMatrix.SetFromXM(DirectX::XMMatrixOrthographicLH(width, height, nearPlane, farPlane));
}
