#include "RenderItem.h"


// TODO: Make 0 the invalid handle
RenderItem::RenderItem() : mesh(-1)
{
}

RenderItem::RenderItem(RenderItem&& other)
{
	transform = other.transform;
	mesh = other.mesh;
}

RenderItem& RenderItem::operator=(RenderItem&& other)
{
	transform = other.transform;
	mesh = other.mesh;

	return *this;
}