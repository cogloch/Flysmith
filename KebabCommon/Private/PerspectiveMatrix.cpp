#include "PerspectiveMatrix.h"


PerspectiveMatrix::PerspectiveMatrix(float width, float height, float fovY, float nearPlane, float farPlane)
{
	m_projMatrix.SetFromXM(DirectX::XMMatrixPerspectiveFovLH(fovY, width / height, nearPlane, farPlane));
}
