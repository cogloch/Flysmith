#include "Vector.h"


Vector2::Vector2() : x(0.0f), y(0.0f) {}

Vector2::Vector2(float x_, float y_) : x(x_), y(y_) {}
Vector2::Vector2(I32 x_, I32 y_) : x(static_cast<float>(x_)), y(static_cast<float>(y_)) {}
Vector2::Vector2(U32 x_, U32 y_) : x(static_cast<float>(x_)), y(static_cast<float>(y_)) {}

Vector2::Vector2(float uniformValue) : x(uniformValue), y(uniformValue) {}

Vector2 Vector2::operator+=(const Vector2& other)
{
	x += other.x;
	y += other.y;
	return *this;
}

Vector2 Vector2::operator*=(float scale)
{
	x *= scale;
	y *= scale;
	return *this;
}

bool operator!=(const Vector2& lhs, const Vector2& rhs)
{
	return !(lhs.x == rhs.x && lhs.y == rhs.y);
}

bool operator!=(const Vector3& lhs, const Vector3& rhs)
{
	return !(lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z);
}

bool operator==(const Vector2& lhs, const Vector2& rhs)
{
	return (lhs.x == rhs.x && lhs.y == rhs.y);
}

bool operator==(const Vector3&lhs , const Vector3& rhs)
{
	return (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z);
}

Vector3::Vector3() : x(0.0f), y(0.0f), z(0.0f) {}

Vector3::Vector3(float x_, float y_, float z_) : x(x_), y(y_), z(z_) {}

Vector3::Vector3(float uniformValue) : x(uniformValue), y(uniformValue), z(uniformValue) {}

Vector3::Vector3(Vector2 vec, float z_) : x(vec.x), y(vec.y), z(z_) {}

Vector3& Vector3::operator=(const Vector3& other)
{
	x = other.x;
	y = other.y;
	z = other.z;
	return *this;
}

Vector3 Vector3::operator+(const Vector3& other) const
{
	return Vector3(x + other.x, y + other.y, z + other.z);
}

Vector3 Vector3::operator-(const Vector3& other) const
{
	return Vector3(x - other.x, y - other.y, z - other.z);
}

Vector3 Vector3::operator*(float scale) const
{
	return Vector3(x * scale, y * scale, z * scale);
}

Vector3 Vector3::operator/(float scale) const
{
	return Vector3(x / scale, y / scale, z / scale);
}

Vector3 Vector3::operator+=(const Vector3& other)
{
	x += other.x;
	y += other.y;
	z += other.z;
	return *this;
}

Vector3 Vector3::operator-=(const Vector3& other)
{
	x -= other.x;
	y -= other.y;
	z -= other.z;
	return *this;
}

Vector3 Vector3::operator*=(float scale)
{
	x *= scale;
	y *= scale;
	z *= scale;
	return *this;
}

Vector3 Vector3::operator/=(float scale)
{
	x /= scale;
	y /= scale;
	z /= scale;
	return *this;
}

float Vector3::GetMagnitude() const
{
	return sqrtf(x*x + y*y + z*z);
}

float Vector3::GetMagnitudeSq() const
{
	return (x*x + y*y + z*z);
}

void Vector3::Normalize()
{
	float magnitude = GetMagnitude();
	if (magnitude != 0)
	{
		x /= magnitude;
		y /= magnitude;
		z /= magnitude;
	}
}

DirectX::XMVECTOR Vector2::GetXMVec() const
{
	return DirectX::XMVectorSet(x, y, 0.0f, 0.0f);
}

void Vector2::Set(DirectX::CXMVECTOR vec)
{
	x = DirectX::XMVectorGetX(vec);
	y = DirectX::XMVectorGetY(vec);
}

DirectX::XMVECTOR Vector3::GetXMVec() const
{
	return DirectX::XMVectorSet(x, y, z, 0.0f);
}

void Vector3::Set(DirectX::CXMVECTOR vec)
{
	x = DirectX::XMVectorGetX(vec);
	y = DirectX::XMVectorGetY(vec);
	z = DirectX::XMVectorGetZ(vec);
}

Vector2 operator*(float scale, const Vector2& vec)
{
	return Vector2(vec.x * scale, vec.y * scale);
}

Vector3 operator*(float scale, const Vector3& vec)
{
	return Vector3(vec.x * scale, vec.y * scale, vec.z * scale);
}
