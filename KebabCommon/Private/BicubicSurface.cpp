#include "BicubicSurface.h"
#include "Matrix.h"
#include "Bezier.h"


BicubicSurfacePatch::BicubicSurfacePatch(CubicCurveClass curveClass)
	: m_bSetControlPoints(false)
	, m_class(curveClass)
{
}

void BicubicSurfacePatch::SetClass(CubicCurveClass curveClass)
{
	if (curveClass != m_class)
	{
		m_class = curveClass;
		ComputeIndependentMat();
	}
}

void BicubicSurfacePatch::SetControlPoints(const std::array<Vector3, 16>& p)
{
	bool bSame = true;
	for (auto& point : p)
	{
		for (auto& existingPoint : m_controlPoints)
		{
			if (point != existingPoint)
			{
				bSame = false;
				break;
			}
		}
	}

	if (bSame) return;

	for (int i = 0; i < 16; ++i)
		m_controlPoints[i] = p[i];

	ComputeIndependentMat();

	m_bSetControlPoints = true;
}

std::vector<Vector3> BicubicSurfacePatch::GetSamples(U32 numSamplesPerAxis)
{
	assert(m_bSetControlPoints);
	
	std::vector<Vector3> samples;
	samples.resize(numSamplesPerAxis * numSamplesPerAxis);

	float increment = 1.f / static_cast<float>(numSamplesPerAxis);
	for (float s = .0f; s <= 1.f; s += increment)
	{
		for (float t = .0f; t <= 1.f; t += increment)
		{
			samples.push_back(GetSample(s, t));
		}
	}

	return samples;
}

Vector3 BicubicSurfacePatch::GetSample(float s, float t)
{
	assert(s >= 0 && s <= 1);
	assert(t >= 0 && t <= 1);
	assert(m_bSetControlPoints);

	float smat[4] = { 1, s, s*s, s*s*s },
		  tmat[4] = { 1, t, t*t, t*t*t };

	Vector3 point;
	
	// smat * independent * tmat = 
	// tmat[0]*(smat[0]*ind[0][0] + smat[1]*ind[1][0] + smat[2]*ind[2][0] + smat[3]*ind[3][0]) + 
	// tmat[1]*(smat[0]*ind[0][1] + smat[1]*ind[1][1] + smat[2]*ind[2][1] + smat[3]*ind[3][1]) + 
	// ...
	for (int i = 0; i < 3; ++i)
	{
		for (int tidx = 0; tidx < 4; ++tidx)
		{
			float sum = 0.f;
			for (int sidx = 0; sidx < 4; ++sidx)
			{
				sum += smat[sidx] * m_independentMat[i].m[sidx][tidx];
			}

			point.val[i] += tmat[tidx] * sum;
		}
	}

	return point;
}

void BicubicSurfacePatch::ComputeIndependentMat()
{
	Matrix basis;
	switch (m_class)
	{
	case CubicCurveClass::BEZIER:
		basis = CubicBezierCurve::GetBasis();
		break;
	}

	enum Coord
	{
		x = 0, y = 1, z = 2
	};

	// One geometrical constraint matrix for each axis.
	Matrix geom[3];
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			geom[x].m[i][j] = m_controlPoints[i * 4 + j].x;
			geom[y].m[i][j] = m_controlPoints[i * 4 + j].y;
			geom[z].m[i][j] = m_controlPoints[i * 4 + j].z;
		}
	}

	Matrix basisTranspose = basis.Transpose();

	// independentMat[axis] = basisTranspose * geomMat[axis] * basis
	// Not looping through axis for readability: (0, 1, 2) vs (x, y, z)
	m_independentMat[x] = basisTranspose * geom[x] * basis;
	m_independentMat[y] = basisTranspose * geom[y] * basis;
	m_independentMat[z] = basisTranspose * geom[z] * basis;
}
