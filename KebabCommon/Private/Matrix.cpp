#include "Matrix.h"
#include <cstring> // memset()


DirectX::XMMATRIX Matrix::GetXMMatrix() const
{
	return DirectX::XMMatrixSet(m11, m12, m13, m14,
								m21, m22, m23, m24,
								m31, m32, m33, m34,
								m41, m42, m43, m44);
}

void Matrix::SetFromXM(DirectX::CXMMATRIX src)
{
	DirectX::XMFLOAT4X4 res;
	DirectX::XMStoreFloat4x4(&res, src);

	m11 = res.m[0][0];
	m12 = res.m[0][1];
	m13 = res.m[0][2];
	m14 = res.m[0][3];

	m21 = res.m[1][0];
	m22 = res.m[1][1];
	m23 = res.m[1][2];
	m24 = res.m[1][3];

	m31 = res.m[2][0];
	m32 = res.m[2][1];
	m33 = res.m[2][2];
	m34 = res.m[2][3];

	m41 = res.m[3][0];
	m42 = res.m[3][1];
	m43 = res.m[3][2];
	m44 = res.m[3][3];
}

Matrix Matrix::operator*(const Matrix& other) const
{
	Matrix result;
	result.SetFromXM(DirectX::XMMatrixMultiply(this->GetXMMatrix(), other.GetXMMatrix()));
	return result;
}

Matrix Matrix::Transpose() const
{
	Matrix result;
	result.SetFromXM(DirectX::XMMatrixTranspose(this->GetXMMatrix()));
	return result;
}

Matrix::Matrix()
{
	memset(m, 0, sizeof(float) * 4 * 4);
}

Matrix::Matrix(float m11_, float m12_, float m13_, float m14_, 
			   float m21_, float m22_, float m23_, float m24_,
			   float m31_, float m32_, float m33_, float m34_,
			   float m41_, float m42_, float m43_, float m44_)
{
	m11 = m11_; m12 = m12_; m13 = m13_; m14 = m14_;
	m21 = m21_; m22 = m22_; m23 = m23_; m24 = m24_;
	m31 = m31_; m32 = m32_; m33 = m33_; m34 = m34_;
	m41 = m41_; m42 = m42_; m43 = m43_; m44 = m44_;
}

DirectX::XMMATRIX Matrix3::GetXMMatrix() const
{
	DirectX::XMFLOAT3X3 mat(m11, m12, m13,
							m21, m22, m23,
							m31, m32, m33);

	return DirectX::XMLoadFloat3x3(&mat);
}

void Matrix3::SetFromXM(DirectX::CXMMATRIX src)
{
	DirectX::XMFLOAT3X3 res;
	DirectX::XMStoreFloat3x3(&res, src);

	m11 = res.m[0][0];
	m12 = res.m[0][1];
	m13 = res.m[0][2];

	m21 = res.m[1][0];
	m22 = res.m[1][1];
	m23 = res.m[1][2];

	m31 = res.m[2][0];
	m32 = res.m[2][1];
	m33 = res.m[2][2];
}

Matrix3::Matrix3()
{
	memset(m, 0, sizeof(float) * 3 * 3);
}

Matrix3x4 KEBABCOMMON_API operator*(const Matrix3x4& lhs, const Matrix& rhs)
{
	Matrix3x4 res;

	for (int i = 0; i < 3; ++i)                              // For each line of lhs
		for (int j = 0; j < 4; ++j)                          // For each column of rhs
			for (int k = 0; k < 4; ++k)                      // Iterate through the chosen line for lhs and chosen column for rhs
				res.m[i][j] += lhs.m[i][k] * rhs.m[k][j];    // Sum the products of the resulting iteration values

	return res;
}

Matrix3x4::Matrix3x4()
{
	memset(m, 0, sizeof(float) * 3 * 4);
}

Matrix3x4::Matrix3x4(float m11_, float m12_, float m13_, float m14_, 
				     float m21_, float m22_, float m23_, float m24_,
				     float m31_, float m32_, float m33_, float m34_)
{
	m11 = m11_; m12 = m12_; m13 = m13_; m14 = m14_;
	m21 = m21_; m22 = m22_; m23 = m23_; m24 = m24_;
	m31 = m31_; m32 = m32_; m33 = m33_; m34 = m34_;
}
