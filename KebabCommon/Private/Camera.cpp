#include "Camera.h"


Camera::Camera() : m_bChanged(true)
{
	CacheViewMatrix();
}

const Matrix& Camera::GetViewMatrix()
{
	if(m_bChanged)
	{
		CacheViewMatrix();
	}

	return m_viewMatrix;
}

const Matrix& Camera::GetViewProjMatrix(const ProjectionMatrix& projMat)
{
	if (m_bChanged)
	{
		CacheViewMatrix();
		m_viewProjMatrix = m_viewMatrix * projMat.GetMatrix();
	}

	return m_viewProjMatrix;
}

void Camera::Update(const TransformNoScale& transform)
{
	m_transform = transform;
	m_bChanged = true;
}

void Camera::CacheViewMatrix()
{
	auto posVec = m_transform.GetPosition();
	auto translateMat = XMMatrixTranslation(-posVec.x, -posVec.y, -posVec.z);
	auto viewMat = translateMat * m_transform.GetRotationQuat().GetMatrixFormXM();
	
	m_viewMatrix.SetFromXM(viewMat);
	
	m_bChanged = false;
}