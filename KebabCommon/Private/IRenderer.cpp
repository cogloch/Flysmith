#include "IRenderer.h"


RenderItemHandle IRenderer::AddRenderItem(ResourceHandle mesh, ResourceHandle vertexShader, ResourceHandle pixelShader)
{
	m_renderItemCacheQueue[m_numRenderItemCacheRequests++] = { mesh, vertexShader, pixelShader };
	return m_numRenderItems + m_numRenderItemCacheRequests - 1;
}

IRenderer::IRenderer() 
	: m_pImpl(nullptr)
	, m_renderQueueEnd(0)
	, m_numRenderItemCacheRequests(0)
	, m_numRenderItems(0)
{
}
