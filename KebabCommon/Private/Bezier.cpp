#include "Bezier.h"


const Matrix CubicBezierCurve::s_basisMat = Matrix(
	1, -3,  3, -1,
	0,  3, -6,  3,
	0,  0,  3, -3,
	0,  0,  0,  1
);

CubicBezierCurve::CubicBezierCurve() : m_bSetControlPoints(false)
{
}

void CubicBezierCurve::SetControlPoints(const std::array<Vector2, 4>& p)
{
	bool bSame = true;
	for (auto& point : p)
	{
		for (auto& existingPoint : m_controlPoints)
		{
			if (point != existingPoint)
			{
				bSame = false;
				break;
			}
		}
	}

	if (bSame) return;

	for (int i = 0; i < 4; ++i)
		m_controlPoints[i] = p[i];

	// Geometry matrix
	// G = [P0 P1 P2 P3] = P0x P1x P2x P3x
	//                     P1y   ...
	Matrix3x4 geomMatrix(p[0].x, p[1].x, p[2].x, p[3].x,
						 p[0].y, p[1].y, p[2].y, p[3].y,
						 0     , 0     , 0     , 0     );

	m_gmMatrix = geomMatrix * s_basisMat;

	m_bSetControlPoints = true;
}

void CubicBezierCurve::GetSamples(std::vector<Vector2>* pOutSamples)
{
	assert(m_bSetControlPoints);

	// Sample(t) = g * m * [1 t t^2 t^3] = gm * [1 t t^2 t^3]
	auto numSamples = pOutSamples->size();
	for (U32 sample = 0; sample < numSamples; ++sample)
	{
		float t = 1.0f / static_cast<float>(numSamples) * static_cast<float>(sample);
		float tmat[4] = { 1, t, t*t, t*t*t };

		for (int i = 0; i < 4; ++i)
		{
			pOutSamples->at(sample).x += m_gmMatrix(0, i) * tmat[i];
			pOutSamples->at(sample).y += m_gmMatrix(1, i) * tmat[i];
		}
	}
}

Vector2 CubicBezierCurve::GetSample(float t)
{
	assert(m_bSetControlPoints);
	assert(t >= 0 && t <= 1);
	
	// Sample(t) = g * m * [1 t t^2 t^3] = gm * [1 t t^2 t^3]
	Vector2 sample;
	float tmat[4] = { 1, t, t*t, t*t*t };

	for (int i = 0; i < 4; ++i)
	{
		sample.x += m_gmMatrix(0, i) * tmat[i];
		sample.y += m_gmMatrix(1, i) * tmat[i];
	}

	return sample;
}
