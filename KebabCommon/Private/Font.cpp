#include "Font.h"
#include <cassert>
#include <algorithm>
#include <ft2build.h>
#include FT_FREETYPE_H


const U32 MAX_ROW_WIDTH = 1024;

Font::~Font()
{
	for(auto& glyph : glyphs)
	{
		delete[] glyph.data;
	}
}

bool Font::LoadFromFile(const char* path)
{
	FT_Library library;
	if (!FT_Init_FreeType(&library)) return false;
	
	FT_Face face;
	if (!FT_New_Face(library, path, 0, &face)) return false;
	
	if (!FT_Set_Pixel_Sizes(face, 0, 24)) return false;
	
	U32 rowWidth = 0,
		rowHeight = 0;

	for (FT_ULong c = 32; c < 128; c++) // Skip control codes; start from ' '.
	{
		if (!FT_Load_Char(face, c, FT_LOAD_RENDER)) return false;

		auto ftGlyph = face->glyph;
		auto bmp = ftGlyph->bitmap;

		GlyphGeneric glyph;
		glyph.size    = Vector2(bmp.width, bmp.rows);
		glyph.bearing = Vector2(ftGlyph->bitmap_left, ftGlyph->bitmap_top);
		glyph.advance = Vector2(ftGlyph->advance.x >> 6, ftGlyph->advance.y >> 6);
		
		glyph.data = new unsigned char[bmp.width * bmp.rows];
		memcpy(glyph.data, bmp.buffer, bmp.width * bmp.rows);

		glyphs[c] = std::move(glyph);

		if(rowWidth + bmp.width + 1 >= MAX_ROW_WIDTH)
		{
			atlas.width = std::max(atlas.width, rowWidth);
			atlas.height += rowHeight;
			rowWidth = 0;
			rowHeight = 0;
		}

		rowWidth += bmp.width + 1;
		rowHeight = std::max(rowHeight, bmp.rows);
	}

	atlas.width = std::max(atlas.width, rowWidth);
	atlas.height += rowHeight;

	FT_Done_Face(face);
	FT_Done_FreeType(library);

	return true;
}
