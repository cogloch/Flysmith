#pragma once
#include "Font.h"
#include "Vector.h"


struct Glyph
{
	Vector2 size;
	Vector2 bearing;  // Position relative to the origin(in pixels).
	Vector2 offset;   // Offset of glyph in texture atlas.
	Vector2 advance;  // Distance from the origin to the origin of the next glyph(in 1/64 pixels).
};

struct FontGL
{
	void CreateFromFont(const Font&);
	Glyph glyphs[128];

	U32 atlasTexture;
	F32 atlasWidth;
	F32 atlasHeight;
};

