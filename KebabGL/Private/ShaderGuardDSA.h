#pragma once
#include "Types.h"
#include "Vector.h"
#include "Matrix.h"
#include "PCH.h"


struct ShaderGuardDSA
{
public:
	ShaderGuardDSA(U32 programId)
	{
		assert(gl::IsProgram(programId));
		this->programId = programId;
	}

	// Use non-cached uniform locations.
	void SetUniform(const std::string& locName, float v0) const                     { gl::ProgramUniform1f(programId, GetUniformLoc(locName), v0); }
	void SetUniform(const std::string& locName, float v0, float v1) const           { gl::ProgramUniform2f(programId, GetUniformLoc(locName), v0, v1); }
	void SetUniform(const std::string& locName, float v0, float v1, float v2) const { gl::ProgramUniform3f(programId, GetUniformLoc(locName), v0, v1, v2); }

	void SetUniform(const std::string& locName, I32 v0) const                 { gl::ProgramUniform1i(programId, GetUniformLoc(locName), v0); }
	void SetUniform(const std::string& locName, I32 v0, I32 v1) const         { gl::ProgramUniform2i(programId, GetUniformLoc(locName), v0, v1); }
	void SetUniform(const std::string& locName, I32 v0, I32 v1, I32 v2) const { gl::ProgramUniform3i(programId, GetUniformLoc(locName), v0, v1, v2); }

	void SetUniform(const std::string& locName, const Vector2& vec) const { gl::ProgramUniform2fv(programId, GetUniformLoc(locName), 1, vec.val); }
	void SetUniform(const std::string& locName, const Vector3& vec) const { gl::ProgramUniform3fv(programId, GetUniformLoc(locName), 1, vec.val); }

	void SetUniform(const std::string& locName, const Matrix3& mat, GLboolean transpose = false) const { gl::ProgramUniformMatrix4fv(programId, GetUniformLoc(locName), 1, transpose, &(mat.M[0].x)); }
	void SetUniform(const std::string& locName, const Matrix&  mat, GLboolean transpose = false) const { gl::ProgramUniformMatrix4fv(programId, GetUniformLoc(locName), 1, transpose, &(mat.M[0].x)); }

	// Use cached uniform locations.
	void SetUniform(U32 loc, float v0) const                     { gl::ProgramUniform1f(programId, loc, v0); }
	void SetUniform(U32 loc, float v0, float v1) const           { gl::ProgramUniform2f(programId, loc, v0, v1); }
	void SetUniform(U32 loc, float v0, float v1, float v2) const { gl::ProgramUniform3f(programId, loc, v0, v1, v2); }

	void SetUniform(U32 loc, I32 v0) const                 { gl::ProgramUniform1i(programId, loc, v0); }
	void SetUniform(U32 loc, I32 v0, I32 v1) const         { gl::ProgramUniform2i(programId, loc, v0, v1); }
	void SetUniform(U32 loc, I32 v0, I32 v1, I32 v2) const { gl::ProgramUniform3i(programId, loc, v0, v1, v2); }

	void SetUniform(U32 loc, const Vector2& vec) const { gl::ProgramUniform2fv(programId, loc, 1, vec.val); }
	void SetUniform(U32 loc, const Vector3& vec) const { gl::ProgramUniform3fv(programId, loc, 1, vec.val); }

	void SetUniform(U32 loc, const Matrix3& mat, GLboolean transpose = false) const { gl::ProgramUniformMatrix3fv(programId, loc, 1, transpose, &(mat.M[0].x)); }
	void SetUniform(U32 loc, const Matrix&  mat, GLboolean transpose = false) const { gl::ProgramUniformMatrix4fv(programId, loc, 1, transpose, &(mat.M[0].x)); }

	// Used to cache uniform locations.
	U32 GetUniformLoc(const std::string& name) const
	{
		auto loc = gl::GetUniformLocation(programId, name.c_str());
		assert(loc != -1);
		return loc;
	}

private:
	U32 programId;
};