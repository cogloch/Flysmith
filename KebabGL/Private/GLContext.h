#pragma once


class GLContext
{
public:
	GLContext();
	~GLContext();

	void Init(HWND, U32 width, U32 height, U32 majorVersion = 4, U32 minorVersion = 3);

	// TODO: Add clear flags
	static void ClearBackBuffer();
	static void ClearBackBuffer(float r, float g, float b, float a);
	void SwapContextBuffers() const;

	static void Resize(U32 width, U32 height);

private:
	HWND  m_hWindow;
	HDC   m_hDeviceContext;
	HGLRC m_hRenderContext;

	static bool s_bLoadedFunctions;
	// Returns false on failure.
	bool LoadFunctions() const;

private:
	void CreatePixelFormat();
	void CreateContext(U32 majorVersion, U32 minorVersion);
	void DestroyContext();
};