#pragma once


namespace TextureLoader
{
	// Returns false on failure.
	bool Load(const std::string& filename, int& outWidth, int& outHeight, unsigned char** ppOutBuffer);
};