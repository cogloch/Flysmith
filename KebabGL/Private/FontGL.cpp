#include "PCH.h"
#include "FontGL.h"
#include <algorithm>


const U32 MAX_ROW_WIDTH = 1024;

void FontGL::CreateFromFont(const Font& src)
{
	atlasWidth = src.atlas.width;
	atlasHeight = src.atlas.height;

	gl::GenTextures(1, &atlasTexture);
	gl::ActiveTexture(gl::TEXTURE0);
	gl::BindTexture(gl::TEXTURE_2D, atlasTexture);
	gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RED, atlasWidth, atlasHeight, 0, gl::RED, gl::UNSIGNED_BYTE, nullptr);

	gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);
	
	gl::TextureParameteri(atlasTexture, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE);
	gl::TextureParameteri(atlasTexture, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE);
	gl::TextureParameteri(atlasTexture, gl::TEXTURE_MIN_FILTER, gl::LINEAR);
	gl::TextureParameteri(atlasTexture, gl::TEXTURE_MAG_FILTER, gl::LINEAR);
	
	Vector2 offset;
	U32 rowh = 0;
	for (auto i = 32; i < 128; ++i) // Skip control codes; start from ' '.
	{
		glyphs[i].size    = src.glyphs[i].size;
		glyphs[i].bearing = src.glyphs[i].bearing;
		glyphs[i].advance = src.glyphs[i].advance;
		
		if(offset.x + glyphs[i].size.x + 1 >= MAX_ROW_WIDTH)
		{
			offset.y += rowh;
			rowh = 0;
			offset.x = 0;
		}

		//gl::TexSubImage2D(gl::TEXTURE_2D, 0, offset, 0, glyphs[i].size.x, glyphs[i].size.y, gl::RED, gl::UNSIGNED_BYTE, src.glyphs[i].data);
		gl::TextureSubImage2D(atlasTexture, 0, offset.x, offset.y, glyphs[i].size.x, glyphs[i].size.y, gl::RED, gl::UNSIGNED_BYTE, src.glyphs[i].data);

		GLenum err;
		if(err = gl::GetError())
		{
			int x = 5;
		}

		glyphs[i].offset.x = offset.x / atlasWidth;
		glyphs[i].offset.y = offset.y / atlasHeight;

		rowh = rowh > glyphs[i].size.y ? rowh : glyphs[i].size.y;
		offset.x += glyphs[i].size.x + 1;
	}
	
	gl::BindTexture(gl::TEXTURE_2D, 0);
}
