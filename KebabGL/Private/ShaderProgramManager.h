#pragma once
#include "ShaderProgram.h"


class ResourceCache;

class ShaderProgramManager
{
public:
	ShaderProgramManager(ResourceCache*);
	~ShaderProgramManager();

	// Only vertex and pixel/fragment shader stages are mandatory.
	// Leave unused stages as -1.
	// Having an (optional) tesselation control shader implies having a tesselation evaluation shader also.
	U32 GetShaderProgram(ResourceHandle vertShader, ResourceHandle pixelShader,
						 ResourceHandle computeShader = -1, ResourceHandle geometryShade = -1,
						 ResourceHandle tessControlShader = -1, ResourceHandle tessEvalShader = -1);

private:
	static bool s_bInstantiated;
	ResourceCache* m_pResCache;
	std::vector<ShaderProgram> m_shaderPrograms;
};