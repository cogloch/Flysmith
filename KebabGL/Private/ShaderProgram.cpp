#include "PCH.h"
#include "ShaderProgram.h"
#include "ShaderStage.h"


ShaderProgram::ShaderProgram()
	: m_programId(0)
	, m_bReleaseable(true)
	, m_vertShaderId(-1)
	, m_fragShaderId(-1)
	, m_geometryShaderId(-1)
	, m_tessControlShaderId(-1)
	, m_tessEvalShaderId(-1)
	, m_computeShaderId(-1)
{
}

ShaderProgram::ShaderProgram(ShaderProgram& other)
{
	other.m_bReleaseable = false;
	m_programId = other.m_programId;
	m_vertShaderId = other.m_vertShaderId;
	m_fragShaderId = other.m_fragShaderId;
	m_tessControlShaderId = other.m_tessControlShaderId;
	m_tessEvalShaderId = other.m_tessEvalShaderId;
	m_geometryShaderId = other.m_geometryShaderId;
	m_computeShaderId = other.m_computeShaderId;
}

ShaderProgram& ShaderProgram::operator=(ShaderProgram& other)
{
	other.m_bReleaseable = false;
	m_programId = other.m_programId;
	m_vertShaderId = other.m_vertShaderId;
	m_fragShaderId = other.m_fragShaderId;
	m_tessControlShaderId = other.m_tessControlShaderId;
	m_tessEvalShaderId = other.m_tessEvalShaderId;
	m_geometryShaderId = other.m_geometryShaderId;
	m_computeShaderId = other.m_computeShaderId;
	return *this;
}

// TODO: Error handling 
bool ShaderProgram::Create(I32 vertShaderId, I32 fragShaderId, I32 geomShaderId, I32 tessControlShaderId, I32 tessEvalShaderId, I32 computeShaderId)
{
	m_vertShaderId = vertShaderId;
	m_fragShaderId = fragShaderId;
	m_tessControlShaderId = tessControlShaderId;
	m_tessEvalShaderId = tessEvalShaderId;
	m_geometryShaderId = geomShaderId;
	m_computeShaderId = computeShaderId;

	m_programId = gl::CreateProgram();

	// Configure the pipelne
	std::vector<ShaderStage> pipeline;

	// Mandatory stages
	if (!gl::IsShader(vertShaderId) || !gl::IsShader(fragShaderId)) return false;
	gl::AttachShader(m_programId, vertShaderId);
	gl::AttachShader(m_programId, fragShaderId);

	// Optional stages
	if (geomShaderId >= 0)
	{
		if (!gl::IsShader(geomShaderId)) return false;
		gl::AttachShader(m_programId, geomShaderId);
	}

	if(computeShaderId >= 0)
	{
		if (!gl::IsShader(computeShaderId)) return false;
		gl::AttachShader(m_programId, computeShaderId);
	}

	if(tessControlShaderId >= 0 || tessEvalShaderId >= 0)
	{
		if (tessControlShaderId < 0 || tessEvalShaderId < 0) return false;
		if (!gl::IsShader(tessControlShaderId) || !gl::IsShader(tessEvalShaderId)) return false;

		gl::AttachShader(m_programId, tessControlShaderId);
		gl::AttachShader(m_programId, tessEvalShaderId);
	}

	gl::LinkProgram(m_programId);

	GLint status = 0;
	gl::GetProgramiv(m_programId, gl::LINK_STATUS, &status);
	if (status == 0)
	{
		char buffer[512];
		gl::GetProgramInfoLog(m_programId, 512, nullptr, buffer);
		assert(status != 0);
		//throw "Shader linking error: " + errorMsg + "\n";
		return false;
	}

	return true;
}

ShaderProgram::~ShaderProgram()
{
	if (m_bReleaseable && gl::IsProgram(m_programId))
	{
		gl::DeleteProgram(m_programId);
	}
}