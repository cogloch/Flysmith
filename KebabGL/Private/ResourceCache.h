#pragma once
#include "ShaderTypes.h"
#include "ShaderStage.h"
#include "MeshGL.h"
#include "FontGL.h"


// TODO: Change how handles work
// i.e. have some templated handle class with a dummy tag parameter for what type of object that handle.. handles,
// having multiple types of handles, instead of the unique typedef - ResourceHandle
// e.g. Handle<tagShader>, Handle<tagMesh>; then this could be typedef'd to obtain something cleaner:
//      using ShaderHandle = Handle<tagShader>; or similar to HWND, HINSTANCE, etc.: using HShader = Handle<tagShader>;
//
// TODO: Release shader resources when needed
class ResourceCache
{
public:
	ResourceCache();
	~ResourceCache();
	
	ResourceHandle AddShader(ShaderType, const std::wstring& fullPath);
	ResourceHandle AddMesh(const VertexVec&, const IndexVec&);
	ResourceHandle AddFont(const Font&);

	void UpdateMesh(ResourceHandle meshHandle, const VertexVec&, const IndexVec&);

	bool ExistsShader(ResourceHandle handle) const { return (m_shaders.size() > handle); }
	bool ExistsMesh(ResourceHandle handle)   const { return (m_meshes.size() > handle); }

	FontGL GetFont(ResourceHandle handle) const { return m_fonts[handle]; }
	U32 GetShader(ResourceHandle handle)  const { return m_shaders[handle].GetShaderObject(); }
	U32 GetMeshVAO(ResourceHandle handle) const { return m_meshes[handle].GetVAO(); }
	const MeshGL& GetMesh(ResourceHandle handle) const { return m_meshes[handle]; }
	U32 GetMeshNumIndices(ResourceHandle handle) const { return m_meshes[handle].GetNumIndices(); }

private:
	using ShaderVec = std::vector<ShaderStage>;
	using MeshVec   = std::vector<MeshGL>;
	using FontVec   = std::vector<FontGL>;

	ShaderVec m_shaders;
	MeshVec   m_meshes;
	FontVec   m_fonts;

	static bool s_bInstantiated;
};