#include "PCH.h"
#include "Renderer.h"
#include "Dependencies\GLCore.h"
#include "RendererImpl.h"


#pragma comment(lib, "opengl32.lib")

// TEMP
#include "TextureLoader.h"
U32 testTexture;

Renderer::Renderer(HWND windowHandle, const U32 windowWidth, const U32 windowHeight)
{
	m_pImpl = new Impl(windowHandle, windowWidth, windowHeight, this);
	uiRenderer.m_pResCache = &m_pImpl->m_resCache;
	uiRenderer.m_pShaderManager = &m_pImpl->m_shaderManager;

	/*unsigned char* buff = nullptr;
	int width, height;
	TextureLoader::Load(R"(C:\Users\cogloch\Documents\Projects\Flysmith\Assets\black_white.jpg)", width, height, &buff);

	gl::CreateTextures(gl::TEXTURE_2D, 1, &testTexture);
	gl::BindTexture(gl::TEXTURE_2D, testTexture);
	gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGB, width, height, 0, gl::RGB, gl::UNSIGNED_BYTE, buff);
	gl::GenerateMipmap(gl::TEXTURE_2D);
	gl::BindTexture(gl::TEXTURE_2D, 0);*/
	// Release texture data 
}

Renderer::~Renderer()
{
	if (m_pImpl)
	{
		delete m_pImpl;
		m_pImpl = nullptr;
	}
}

void Renderer::Render() const
{
	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT | gl::STENCIL_BUFFER_BIT);
	
	m_pImpl->Render();
	uiRenderer.Render();

	m_pImpl->m_context.SwapContextBuffers();
}

U32 Renderer::GetShaderProgram(ResourceHandle vertShader, ResourceHandle pixelShader) const
{
	return m_pImpl->m_shaderManager.GetShaderProgram(vertShader, pixelShader);
}

void Renderer::UpdateScene(std::vector<RenderItemProxy> renderables)
{
	for (size_t i = 0; i < m_numRenderItemCacheRequests; ++i)
	{
		auto& request = m_renderItemCacheQueue[i];
		auto& newRenderItem = m_pImpl->m_renderItems[m_numRenderItems++];
		
		newRenderItem.mesh = request.mesh;
		newRenderItem.shaderProgramId = GetShaderProgram(request.vertShader, request.pixelShader);
	}
	m_numRenderItemCacheRequests = 0;

	m_renderQueueEnd = 0;
	for (auto& renderable : renderables)
	{
		auto itemHandle = renderable.renderItemId;
		m_pImpl->m_renderItems[itemHandle].transform = renderable.transform;
		m_renderQueue[m_renderQueueEnd++] = itemHandle;
	}
}

void Renderer::UpdateView(const TransformNoScale& camTransform) const
{
	m_pImpl->m_camera.Update(camTransform);
}

ResourceHandle Renderer::CacheMesh(const VertexVec& verts, const IndexVec& indices) const
{
	return m_pImpl->m_resCache.AddMesh(verts, indices);
}

void Renderer::UpdateMesh(ResourceHandle handle, const VertexVec& verts, const IndexVec& indices) const
{
	return m_pImpl->m_resCache.UpdateMesh(handle, verts, indices);
}

ResourceHandle Renderer::CacheShader(ShaderType type, const std::wstring& fullPath) const
{
	return m_pImpl->m_resCache.AddShader(type, fullPath.c_str());
}

ResourceHandle Renderer::CacheFont(const Font& font) const
{
	return m_pImpl->m_resCache.AddFont(font);
}
