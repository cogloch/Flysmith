#include "PCH.h"
#include "RendererImpl.h"



IRenderer::Impl::Impl(HWND windowHandle, const U32 windowWidth, const U32 windowHeight, IRenderer* pRenderer)
	: m_shaderManager(&m_resCache)
	, m_pRenderer(pRenderer)
{
	m_context.Init(windowHandle, 800, 600, 4, 5);
	gl::Enable(gl::DEPTH_TEST);
	//gl::Enable(gl::MULTISAMPLE);
}

#include "BicubicSurface.h"
#include "ShaderGuard.h"

void IRenderer::Impl::Render()
{
	// TODO: cache
	PerspectiveMatrix perspectiveMat(800.0f, 600.0f, 0.8f, 1.0f, 1000.0f);

	auto viewProjMat = m_camera.GetViewProjMatrix(perspectiveMat);
	auto viewMat = m_camera.GetViewMatrix();
	auto projMat = perspectiveMat.GetMatrix();

	Vector3 camPos;
	camPos.Set(DirectX::XMLoadFloat3(&m_camera.GetTransform().GetPosition()));

	for (U32 i = 0; i < m_pRenderer->m_renderQueueEnd; i++)
	{
		m_renderItems[m_pRenderer->m_renderQueue[i]].Render(viewMat, projMat, &m_resCache, camPos);
	}

	// TEMP
	static U32 vao, vbo;
	static bool bInit = false;
	static ShaderProgram prog;
	static ShaderStage vs, fs;
	static const U32 numSamples = 10;
	if (!bInit)
	{
		gl::CreateBuffers(1, &vbo);
		gl::CreateVertexArrays(1, &vao);

		gl::BindVertexArray(vao);
		gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
		gl::BufferData(gl::ARRAY_BUFFER, numSamples * numSamples * sizeof(Vector3), nullptr, gl::DYNAMIC_DRAW);

		gl::EnableVertexAttribArray(0);
		gl::VertexAttribPointer(0, 3, gl::FLOAT, 0, sizeof(Vector3), (void*)0);

		vs.CreateFromFile(LR"(D:\Flysmith\Assets\Shaders\PatchVS.glsl)", ShaderType::VERTEX_SHADER);
		fs.CreateFromFile(LR"(D:\Flysmith\Assets\Shaders\PatchPS.glsl)", ShaderType::PIXEL_SHADER);
		auto vsId = vs.GetShaderObject(), fsId = fs.GetShaderObject();
		prog.Create(vsId, fsId);

		bInit = true;
	}

	static bool bChanged = true;
	static BicubicSurfacePatch patch(CubicCurveClass::BEZIER);
	std::vector<Vector3> samples(numSamples * numSamples);
	if (bChanged)
	{
		std::ifstream spinnerFile(R"(D:\Flysmith\Assets\Aircraft\Elements\Fairings\Spinner1)");
		std::array<Vector3, 16> controlPoints;
		for (int i = 0; i < 16; ++i)
			spinnerFile >> controlPoints[i].x >> controlPoints[i].y >> controlPoints[i].z;

		patch.SetControlPoints(controlPoints);
		samples = patch.GetSamples(numSamples);

		bChanged = false;
	}

	ShaderGuard shaderGuard(prog.GetProgramId());
	shaderGuard.SetUniform("viewProjMatrix", viewProjMat);
	gl::BindVertexArray(vao);
	gl::DrawArrays(gl::POINT, 0, numSamples * numSamples);
	gl::BindVertexArray(0);
}