#pragma once
#include "RenderItem.h"
#include "Vector.h"


class ResourceCache;

struct RenderItemGL : RenderItem
{
	RenderItemGL() = default;
	void Render(const Matrix& viewMat, const Matrix& projMat, ResourceCache*, const Vector3& camPos) const;

	U32 shaderProgramId;
};