#include "PCH.h"
#include "RendererUI.h"

// TEMP
#include "TextRenderer.h"
#include "ResourceCache.h"
#include "ShaderProgramManager.h"

// TEMP
std::vector<Label> labels;

BatchId RendererUI::RenderLabel(const std::string& text, U32 fontId, Vector2 pos, Vector3 color)
{
	// TEMP
	labels.push_back({ text, fontId, pos, color });

	auto font = m_pResCache->GetFont(fontId);
	return 0;
}

void RendererUI::UpdateLabel(BatchId id, const std::string& text, U32 fontId, Vector2 pos, Vector3 color)
{
	auto font = m_pResCache->GetFont(fontId);
}

void RendererUI::RemoveLabel(BatchId id)
{
	size_t line, offset;
	if(GetBatch(id, line, offset))
	{
		auto batchLine = m_batchDataVec[line].batches;
		batchLine.erase(batchLine.begin() + offset);
	}
}

#include "OrthographicMatrix.h"
#include "ShaderGuard.h"
#include "ShaderStage.h"
#include "Bezier.h"
#include <array>

// Cubic Bezier
void DrawBezier(const std::array<Vector2, 4>& p)
{
	static U32 vaoCurve, vaoCtrlPoints, vbo;
	static ShaderProgram prog;
	static ShaderStage vs, fs;
	static bool bInit = false;
	static const U32 numSamples = 32;

	if (!bInit)
	{
		gl::CreateBuffers(1, &vbo);
		gl::CreateVertexArrays(1, &vaoCurve);

		gl::BindVertexArray(vaoCurve);
		gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
		gl::BufferData(gl::ARRAY_BUFFER, numSamples * sizeof(Vector2), nullptr, gl::DYNAMIC_DRAW);

		gl::EnableVertexAttribArray(0);
		gl::VertexAttribPointer(0, 2, gl::FLOAT, 0, sizeof(Vector2), (void*)0);

		vs.CreateFromFile(LR"(D:\Flysmith\Assets\Shaders\CurveVS.glsl)", ShaderType::VERTEX_SHADER);
		fs.CreateFromFile(LR"(D:\Flysmith\Assets\Shaders\CurvePS.glsl)", ShaderType::PIXEL_SHADER);
		auto vsId = vs.GetShaderObject(), fsId = fs.GetShaderObject();
		prog.Create(vsId, fsId);

		bInit = true;
	}

	static bool bChanged = true;
	static CubicBezierCurve curve;
	std::vector<Vector2> samples(numSamples);
	if (bChanged)
	{
		curve.SetControlPoints(p);
		curve.GetSamples(&samples);
		gl::BufferData(gl::ARRAY_BUFFER, numSamples * sizeof(Vector2), &samples[0], gl::DYNAMIC_DRAW);
		bChanged = false;
	}

	OrthographicMatrix ortho(800, 600);
	ShaderGuard shaderGuard(prog.GetProgramId());
	shaderGuard.SetUniform("proj", ortho.GetMatrix());
	gl::BindVertexArray(vaoCurve);
	gl::DrawArrays(gl::LINE_STRIP, 0, numSamples);
	gl::BindVertexArray(0);
}

void RendererUI::Render() const
{
	SetServerState();

	auto textShader = m_pShaderManager->GetShaderProgram(2, 3);
	
	for (auto& batchLine : m_batchDataVec)
	{
		// Generate/update vertex array objs for each batch line 
		// Render
	}

	for(auto& label : labels)
	{
		TextRenderer::Render(textShader, m_pResCache->GetFont(label.fontId), label.text, label.pos, 1, label.color);
	}

	labels.clear();

	std::array<Vector2, 4> ctrlPoints;
	ctrlPoints[0] = { 3.71f, 0.52f };
	ctrlPoints[1] = { 13.25f, 10.17f };
	ctrlPoints[2] = { 8.59f, 2.16f };
	ctrlPoints[3] = { 3.76f, 0.55f };

	DrawBezier(ctrlPoints);

	RestoreServerState();
}

void RendererUI::SetServerState()
{
	gl::Disable(gl::DEPTH_TEST);
	gl::Enable(gl::BLEND);
	gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
}

void RendererUI::RestoreServerState()
{
	gl::Disable(gl::BLEND);
	gl::Enable(gl::DEPTH_TEST);
}

bool RendererUI::GetBatch(BatchId id, size_t& line, size_t& offset)
{
	auto numBatchLines = m_batchDataVec.size();
	for (line = 0; line < numBatchLines; ++line)
	{
		auto numBatches = m_batchDataVec[line].batches.size();
		for (offset = 0; offset < numBatches; ++offset)
		{
			if(m_batchDataVec[line].batches[offset].id == id)
			{
				return true;
			}
		}
	}

	return false;
}
