#include "PCH.h"
#include "TextureLoader.h"
#include "Dependencies/SOIL/SOIL.h"


bool TextureLoader::Load(const std::string& filename, int& outWidth, int& outHeight, unsigned char** ppOutBuffer)
{
	*ppOutBuffer = SOIL_load_image(filename.c_str(), &outWidth, &outHeight, 0, SOIL_LOAD_RGB);
	return (*ppOutBuffer != nullptr);
}
