#pragma once
#include "Renderer.h"
#include "GLContext.h"

#include "Camera.h"
#include "PerspectiveMatrix.h"
#include "ResourceCache.h"

#include "RenderItemGL.h"
#include "ShaderProgramManager.h"

struct IRenderer::Impl
{
	Impl(HWND windowHandle, const U32 windowWidth, const U32 windowHeight, IRenderer*);

	void Render();

	GLContext m_context;

	// Holds all render items, whether they are to be rendered or not.
	RenderItemGL m_renderItems[MAX_RENDER_ITEMS];
	
	Camera m_camera;

	ResourceCache m_resCache;
	ShaderProgramManager m_shaderManager;

	struct UIRenderItemCacheRequest
	{
		ResourceHandle texture;
	};
	std::vector<UIRenderItemCacheRequest> m_uiRenderItemCacheRequets;

	IRenderer* m_pRenderer;
};