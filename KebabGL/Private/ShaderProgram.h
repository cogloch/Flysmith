#pragma once


class ShaderProgram
{
public:
	ShaderProgram();
	~ShaderProgram();

	ShaderProgram(ShaderProgram&);
	ShaderProgram& operator=(ShaderProgram&);

	// Returns a reference to an Opengl program object(the object's name)
	U32 GetProgramId() const { return m_programId; }

	// Only vertex and pixel/fragment shader stages are mandatory.
	// Leave unused stages as -1.
	// Having an (optional) tesselation control shader implies having a tesselation evaluation shader also.
	bool Create(I32 vertShaderId, I32 fragShaderId, I32 geomShaderId = -1, 
				I32 tessControlShaderId = -1, I32 tessEvalShaderId = -1, 
				I32 computeShaderId = -1);

private:
	// Reference to an Opengl shader program
	U32 m_programId;

	// Makes sure the OGL shader program id is not released too early
	bool m_bReleaseable;

	// Reflection
	friend class ShaderProgramManager;
	I32 m_vertShaderId;
	I32 m_fragShaderId;
	I32 m_geometryShaderId;
	I32 m_tessControlShaderId;
	I32 m_tessEvalShaderId;
	I32 m_computeShaderId;
};