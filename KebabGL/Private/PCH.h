#pragma once


#include "Types.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <fstream>
#include <cassert>
#include <string>
#include <vector>
#include <map>

#include "Dependencies\GLCore.h"