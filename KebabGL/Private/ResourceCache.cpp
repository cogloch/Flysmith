#include "PCH.h"
#include "ResourceCache.h"


bool ResourceCache::s_bInstantiated = false;

ResourceCache::ResourceCache()
{
	assert(!s_bInstantiated);
	s_bInstantiated = true;
}

ResourceCache::~ResourceCache()
{
	s_bInstantiated = false;
}

ResourceHandle ResourceCache::AddShader(ShaderType type, const std::wstring& path)
{
	ShaderStage shader;
	if (!shader.CreateFromFile(path, type)) { OutputDebugStringA("Shader creation failed"); /* TODO: Actually handle the error */ }
	m_shaders.emplace_back(shader);
	return m_shaders.size() - 1;
}

ResourceHandle ResourceCache::AddMesh(const VertexVec& verts, const IndexVec& indices)
{
	MeshGL mesh;
	mesh.Create(verts, indices);
	m_meshes.push_back(std::move(mesh));
	return m_meshes.size() - 1;
}

ResourceHandle ResourceCache::AddFont(const Font& font)
{
	FontGL newFont;
	newFont.CreateFromFont(font);
	m_fonts.emplace_back(std::move(newFont));
	return m_fonts.size() - 1;
}

void ResourceCache::UpdateMesh(ResourceHandle meshHandle, const VertexVec& verts, const IndexVec& indices)
{
	m_meshes[meshHandle].Destroy();
	m_meshes[meshHandle].Create(verts, indices);
}
