#include "PCH.h"
#include "ShaderProgramManager.h"
#include "ResourceCache.h"


bool ShaderProgramManager::s_bInstantiated = false;

ShaderProgramManager::ShaderProgramManager(ResourceCache* pResCache)
{
	assert(!s_bInstantiated);
	m_pResCache = pResCache;
	s_bInstantiated = true;
}

ShaderProgramManager::~ShaderProgramManager()
{
	s_bInstantiated = false;
}

U32 ShaderProgramManager::GetShaderProgram(ResourceHandle vertShader, ResourceHandle pixelShader,
										   ResourceHandle computeShader, ResourceHandle geometryShader,
										   ResourceHandle tessControlShader, ResourceHandle tessEvalShader)
{
	I32 vs = -1, ps = -1, cs = -1, gs = -1, tc = -1, te = -1;

	vs = m_pResCache->GetShader(vertShader);
	ps = m_pResCache->GetShader(pixelShader);
	if (computeShader != -1)     cs = m_pResCache->GetShader(computeShader);
	if (geometryShader != -1)    gs = m_pResCache->GetShader(geometryShader);
	if (tessControlShader != -1) tc = m_pResCache->GetShader(tessControlShader);
	if (tessEvalShader != -1)    te = m_pResCache->GetShader(tessEvalShader);

	for(const auto& shader : m_shaderPrograms)
	{
		if(vs == shader.m_vertShaderId && ps == shader.m_fragShaderId && 
		   cs == shader.m_computeShaderId && gs == shader.m_geometryShaderId &&
		   tc == shader.m_tessControlShaderId && te == shader.m_tessEvalShaderId)
		{
			return shader.m_programId;
		}
	}

	ShaderProgram program;
	bool succ = program.Create(vs, ps, gs, tc, te, cs);
	assert(succ);
	m_shaderPrograms.emplace_back(program);

	return program.m_programId;
}
