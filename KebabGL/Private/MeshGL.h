#pragma once
#include "IMesh.h"
#include "Vertex.h"


class MeshGL final : public IMesh
{
public:
	VertexVec verts;
	IndexVec  indices;

public:
	MeshGL();
	void Create(const VertexVec& verts, const IndexVec& indices);
	
	MeshGL& operator=(MeshGL&&);
	MeshGL(MeshGL&&);

	// The resource manager will handle releasing ShaderStages' resource through Destroy() when it absolutely wants to.
	// This way releasing the ogl shader id too early is avoided and tracking a "releaseable" flag is no longer needed. 
	~MeshGL() = default;
	void Destroy();
	
	U32 GetVAO() const                 { return m_vertArrayObject; }
	U32 GetNumIndices() const override { return indices.size(); }

private:
	U32  m_vertArrayObject;
	bool m_bCreated;
};