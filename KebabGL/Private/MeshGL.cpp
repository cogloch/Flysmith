#include "PCH.h"
#include "MeshGL.h"
#include "Dependencies\GLCore.h"


MeshGL::MeshGL() : m_vertArrayObject(0), m_bCreated(false)
{
}

MeshGL::MeshGL(MeshGL&& other)
{
	verts = std::move(other.verts);
	indices = std::move(other.indices);
	m_vertArrayObject = std::move(other.m_vertArrayObject);
	m_bCreated = std::move(other.m_bCreated);
}

MeshGL& MeshGL::operator=(MeshGL&& other)
{
	verts = std::move(other.verts);
	indices = std::move(other.indices);
	m_vertArrayObject = std::move(other.m_vertArrayObject);
	m_bCreated = std::move(other.m_bCreated);
	return *this;
}

void MeshGL::Create(const VertexVec& verts, const IndexVec& indices)
{
	assert(!m_bCreated);

	// Consider not storing vert/index data here(current idea is that they may be updated)
	this->verts = verts;
	this->indices = indices;

	U32 vbo, ebo;

	gl::GenVertexArrays(1, &m_vertArrayObject);
	gl::GenBuffers(1, &vbo);
	gl::GenBuffers(1, &ebo);

	gl::BindVertexArray(m_vertArrayObject);

	gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
	gl::BufferData(gl::ARRAY_BUFFER, verts.size() * sizeof(Vertex), &verts[0], gl::STATIC_DRAW);

	gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ebo);
	gl::BufferData(gl::ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(U32), &indices[0], gl::STATIC_DRAW);

	// TEMP
	// TODO: Separate vertex attribute linking 
	// Position
	gl::EnableVertexAttribArray(0);
	gl::VertexAttribPointer(0, 3, gl::FLOAT, 0, sizeof(Vertex), (void*)(0));

	// Normal
	gl::EnableVertexAttribArray(1);
	gl::VertexAttribPointer(1, 3, gl::FLOAT, 0, sizeof(Vertex), (void*)offsetof(Vertex, normal));

	gl::BindVertexArray(0);

	m_bCreated = true;
}

void MeshGL::Destroy()
{
	if(gl::IsVertexArray(m_vertArrayObject) && m_bCreated)
	{
		gl::DeleteVertexArrays(1, &m_vertArrayObject);
		m_bCreated = false;
	}
}
