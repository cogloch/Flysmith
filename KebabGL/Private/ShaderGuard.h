#pragma once
#include "ShaderProgram.h"
#include "Vector.h"
#include "Matrix.h"


// Non-DSA method 
// ShaderProgram RAII wrapper
// Usage is weird and ugly, but works.
// TODO: Make usage not weird and ugly.
class ShaderGuard
{
public:
	ShaderGuard(const ShaderProgram&);

	// Gets the shader program with the passed name from ResourceManager
	ShaderGuard(const std::string&);

	// Pass an Opengl shader program name
	ShaderGuard(const U32);
	~ShaderGuard();

	// Use non-cached uniform locations.
	void SetUniform(const std::string&, float) const;
	void SetUniform(const std::string&, float, float) const;
	void SetUniform(const std::string&, float, float, float) const;

	void SetUniform(const std::string&, I32) const;
	void SetUniform(const std::string&, I32, I32) const;
	void SetUniform(const std::string&, I32, I32, I32) const;

	void SetUniform(const std::string&, const Vector2&) const;
	void SetUniform(const std::string&, const Vector3&) const;

	void SetUniform(const std::string&, const Matrix3&, GLboolean transpose = false) const;
	void SetUniform(const std::string&, const Matrix&, GLboolean transpose = false) const;

	// Use cached uniform locations.
	void SetUniform(I32 location, float) const;
	void SetUniform(I32 location, float, float) const;
	void SetUniform(I32 location, float, float, float) const;

	void SetUniform(I32 location, I32) const;
	void SetUniform(I32 location, I32, I32) const;
	void SetUniform(I32 location, I32, I32, I32) const;

	void SetUniform(I32 location, const Vector2&) const;
	void SetUniform(I32 location, const Vector3&) const;

	void SetUniform(I32 location, const Matrix3&, GLboolean transpose = false) const;
	void SetUniform(I32 location, const Matrix&, GLboolean transpose = false) const;

	// Used to cache uniform locations.
	I32 GetUniformLocation(const std::string&) const;

public:
	ShaderGuard(ShaderGuard&&) = delete;
	ShaderGuard(const ShaderGuard&) = delete;
	ShaderGuard& operator = (ShaderGuard&&) = delete;
	ShaderGuard& operator = (const ShaderGuard&) = delete;

private:
	U32 m_oglName;

	// Check if there's another bound shader program object, meaning that someone, somewhere, out there
	// isn't using this. Logs the occurence.
	void AssertNoShaderBound();
};