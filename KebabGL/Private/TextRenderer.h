#pragma once
#include <string>
#include "Types.h"
#include "Vector.h"
#include "FontGL.h"


namespace TextRenderer
{
	// NOT responsible for disabling/enabling depth testing and blending
	void Render(U32 spriteShaderProg, const FontGL& font, const std::string& text, Vector2 pos, float scale, Vector3 color);
}