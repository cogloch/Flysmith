#include "PCH.h"
#include "TextRenderer.h"
#include "ShaderGuard.h"
#include "OrthographicMatrix.h"


void TextRenderer::Render(U32 spriteShaderProg, const FontGL& font, const std::string& text, Vector2 pos, float scale, Vector3 color)
{
	ShaderGuard shader(spriteShaderProg);

	OrthographicMatrix ortho(800, 600); // TODO: Cache
	shader.SetUniform("proj", ortho.GetMatrix());
	shader.SetUniform("color", color);

	assert(gl::IsTexture(font.atlasTexture));
	gl::ActiveTexture(gl::TEXTURE0);
	gl::BindTexture(gl::TEXTURE_2D, font.atlasTexture);

	// TODO
	static U32 vbo = 0, vao = 0;
	bool bInit = false;
	if(!bInit)
	{
		gl::CreateBuffers(1, &vbo);
		gl::CreateVertexArrays(1, &vao);
		
		gl::BindVertexArray(vao);
		gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
		bInit = true;
	}
	
	gl::EnableVertexAttribArray(0);
	gl::VertexAttribPointer(0, 4, gl::FLOAT, false, 4 * sizeof(float), nullptr);

	struct TextVert
	{
		float x, y; // Pos coords
		float s, t; // Tex coords
	};
	TextVert* verts = new TextVert[6 * text.size()];
	
	auto x = pos.x,
		 y = pos.y;

	size_t vertIdx = 0;
	for (auto ch = text.begin(), end = text.end(); ch != end; ++ch)
	{
		auto glyph = font.glyphs[*ch];

		auto xpos =  x + glyph.bearing.x * scale,
			 ypos = -y - glyph.bearing.y * scale;
		
		auto w = glyph.size.x * scale,
			 h = glyph.size.y * scale;

		x += glyph.advance.x * scale;
		y += glyph.advance.y * scale;

		if (!w || !h) continue;

		//                    x          y                        s                                       t
		verts[vertIdx++] = { xpos,     -ypos,     glyph.offset.x,                                  glyph.offset.y };
		verts[vertIdx++] = { xpos + w, -ypos,     glyph.offset.x + glyph.size.x / font.atlasWidth, glyph.offset.y };
		verts[vertIdx++] = { xpos,     -ypos - h, glyph.offset.x,                                  glyph.offset.y + glyph.size.y / font.atlasHeight };
		verts[vertIdx++] = { xpos + w, -ypos,     glyph.offset.x + glyph.size.x / font.atlasWidth, glyph.offset.y };
		verts[vertIdx++] = { xpos,     -ypos - h, glyph.offset.x,                                  glyph.offset.y + glyph.size.y / font.atlasHeight };
		verts[vertIdx++] = { xpos + w, -ypos - h, glyph.offset.x + glyph.size.x / font.atlasWidth, glyph.offset.y + glyph.size.y / font.atlasHeight };
	}

	gl::BufferData(gl::ARRAY_BUFFER, 6 * text.size() * sizeof(TextVert), &verts[0], gl::DYNAMIC_DRAW);
	gl::DrawArrays(gl::TRIANGLES, 0, vertIdx);

	gl::BindTexture(gl::TEXTURE_2D, 0);
	gl::DisableVertexAttribArray(0);
	delete[] verts;
}