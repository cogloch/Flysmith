#include "PCH.h"
#include "ShaderStage.h"
#include "FileReader.h"


ShaderStage::ShaderStage() : m_type(INVALID), m_shader(0), m_bCreated(false)
{
}

bool ShaderStage::CreateFromFile(const std::wstring& fullPath, ShaderType type)
{
	if (m_bCreated) return false;

	std::string src;
	if (!FileReader::ReadSync(fullPath, src))
	{
		// throw "File not found: " + filePath + "\n"
		return false;
	}

	return CreateFromSource(src, type);
}

bool ShaderStage::CreateFromSource(const std::string& source, ShaderType type)
{
	if (m_bCreated) return false;

	m_type = type;

	// TODO: Temp
	// ShaderType == 0 is the INVALID shader => indexing starts at 1; ends at 6
	GLenum typesMap[7];
	typesMap[ShaderType::COMPUTE_SHADER] = gl::COMPUTE_SHADER;
	typesMap[ShaderType::VERTEX_SHADER] = gl::VERTEX_SHADER;
	typesMap[ShaderType::HULL_SHADER] = gl::TESS_CONTROL_SHADER;
	typesMap[ShaderType::DOMAIN_SHADER] = gl::TESS_EVALUATION_SHADER;
	typesMap[ShaderType::GEOMETRY_SHADER] = gl::GEOMETRY_SHADER;
	typesMap[ShaderType::PIXEL_SHADER] = gl::FRAGMENT_SHADER;

	m_shader = gl::CreateShader(static_cast<GLenum>(typesMap[m_type]));

	auto shaderCodePtr = source.c_str();
	auto shaderCodeSize = static_cast<GLint>(source.size());

	gl::ShaderSource(m_shader, 1, &shaderCodePtr, &shaderCodeSize);
	gl::CompileShader(m_shader);

	GLint status = 0;
	gl::GetShaderiv(m_shader, gl::COMPILE_STATUS, &status);

	if (status == 0)
	{
		char buffer[512];
		gl::GetShaderInfoLog(m_shader, 512, nullptr, buffer);
		assert(status);
		// throw "Error compiling shader: " + shaderName + ShaderStage::s_GLSLExtensions[shaderType] + " - " + errorMsg + "\n"
		return false;
	}

	// TODO: Should the m_shader id be released if creation failed?

	m_bCreated = true;
	return true;
}

void ShaderStage::Destroy()
{
	if(m_bCreated && gl::IsShader(m_shader))
	{
		gl::DeleteShader(m_shader);
		m_bCreated = false;
	}
}
