#include "PCH.h"
#include "RenderItemGL.h"
#include "ShaderGuard.h"
#include "ResourceCache.h"


void RenderItemGL::Render(const Matrix& viewMat, const Matrix& projMat, ResourceCache* pResCache, const Vector3& camPos) const
{
	ShaderGuard shader(shaderProgramId);

	// Model matrix
	shader.SetUniform("modelMatrix", transform);

	// View-Projection matrix
	shader.SetUniform("viewProjMatrix", viewMat * projMat);

	// Lights
	static float y = 50.0f;
	shader.SetUniform("lightPos", Vector3(0.0f, y, 0.0f));
	shader.SetUniform("viewPos", camPos);
	shader.SetUniform("lightColor", Vector3(0.2f, 1.0f, 0.4f));
	shader.SetUniform("objectColor", Vector3(0.5f, 0.5f, 0.1f));

	auto vao = pResCache->GetMeshVAO(mesh);
	gl::BindVertexArray(vao);
	gl::DrawElements(gl::TRIANGLES, (GLsizei)pResCache->GetMeshNumIndices(mesh), gl::UNSIGNED_INT, 0);
	gl::BindVertexArray(0);
}
