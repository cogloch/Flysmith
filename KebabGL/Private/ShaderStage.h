#pragma once
#include "ShaderTypes.h"


class ShaderStage
{
public:
	ShaderStage();
	// Returns false on failure.
	// Will fail if the current object has already been 
	// TODO: Log/handle errors.
	// TODO: Could also use exceptions, as bad as they are. 
	bool CreateFromFile(const std::wstring& fullPath, ShaderType);
	bool CreateFromSource(const std::string& source, ShaderType);

	// The resource manager will handle releasing ShaderStages' resource through Destroy() when it absolutely wants to.
	// This way releasing the ogl shader id too early is avoided and tracking a "releaseable" flag is no longer needed. 
	~ShaderStage() = default; 
	void Destroy();

	ShaderType GetType() const  { return m_type;   }
	U32 GetShaderObject() const { return m_shader; }

private:
	// OGL shader object id
	U32 m_shader;
	bool m_bCreated;
	ShaderType m_type;
};