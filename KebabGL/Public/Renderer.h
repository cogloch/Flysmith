#pragma once
#include "TransformNoScale.h"
#include "RenderItemProxy.h"
#include "ShaderTypes.h"
#include "PublicDef.h"
#include "IRenderer.h"
#include "Vertex.h"
#include "Font.h"
#include "RendererUI.h"


class KEBABGL_API Renderer final : public IRenderer
{
public:
	Renderer(const HWND windowHandle, const U32 windowWidth, const U32 windowHeight);
	~Renderer();

	// Populate a list of render item proxies from render component data and update 
	// the render state for the next frame.
	void UpdateScene(std::vector<RenderItemProxy>) override;

	// Copy camera transform
	void UpdateView(const TransformNoScale& camTransform) const override;

	void Render() const override;

	// TEMP
	U32 GetShaderProgram(ResourceHandle vertShader, ResourceHandle pixelShader) const;

	// TODO: Queue resource caching requests for later 
	ResourceHandle CacheMesh(const VertexVec& verts, const IndexVec& indices) const override;
	void UpdateMesh(ResourceHandle, const VertexVec& verts, const IndexVec& indices) const override;
	ResourceHandle CacheShader(ShaderType, const std::wstring& fullPath) const override;
	ResourceHandle CacheFont(const Font&) const;

	RendererUI uiRenderer;
};