#pragma once
#include "RenderItemProxy.h"
#include "ShaderTypes.h"
#include "PublicDef.h"
#include "Transform.h"
#include "IRenderer.h"
#include "Vertex.h"


class KEBAB12_API Renderer final : public IRenderer
{
public:
	Renderer(const HWND, const U32 windowWidth, const U32 windowHeight);
	~Renderer();

	// Populate a list of render item proxies from render component data and update 
	// the render state for the next frame.
	void UpdateScene(std::vector<RenderItemProxy>) override;

	// Copy camera transform
	void UpdateView(const TransformNoScale& camTransform) const override;

	void Render() const override;

	// TODO: Queue resource caching requests for later 
	ResourceHandle CacheMesh(const VertexVec& verts, const IndexVec& indices) const override;
	void UpdateMesh(ResourceHandle, const VertexVec& verts, const IndexVec& indices) const override;
	ResourceHandle CacheShader(ShaderType, const std::wstring& fullPath) const override;
};