#pragma once
#include "Resources\Resource.h"
#include "Descriptors\IndexBufferView.h"
#include "Descriptors\VertexBufferView.h"
#include "IMesh.h"
#include "Vertex.h"


class MeshDX12 final : public IMesh
{
public:
	VertexVec verts;
	IndexVec  indices;

public:
	MeshDX12()  = default;
	~MeshDX12() {}

	MeshDX12(MeshDX12&& other);
	MeshDX12& operator=(MeshDX12&& other);

	void Init(ID3D12Device* pDevice);

	VertexBufferView& GetVertBufferView() { return m_vertBufferView;  }
	IndexBufferView& GetIndexBufferView() { return m_indexBufferView; }

	U32 GetNumIndices()    const override { return indices.size();    }

private:
	Resource         m_vertBuffer;
	VertexBufferView m_vertBufferView;

	Resource        m_indexBuffer;
	IndexBufferView m_indexBufferView;
};