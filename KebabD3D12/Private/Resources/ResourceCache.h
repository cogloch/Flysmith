#pragma once
#include "MeshDX12.h"
#include "Pipeline\ShaderProgram.h"
#include "Vertex.h"


// TODO: Handle all resource types uniformly
class ResourceCache
{
public:
	ResourceCache();
	void Init(ID3D12Device*);

	ResourceHandle AddMesh(const VertexVec&, const IndexVec&);
	ResourceHandle AddShader(ShaderType, const wchar_t* path);
	
	void UpdateMesh(ResourceHandle meshHandle, const VertexVec&, const IndexVec&);
	
	bool ExistsMesh(ResourceHandle handle) const   { return m_meshes.size() > handle;  }
	bool ExistsShader(ResourceHandle handle) const { return m_shaders.size() > handle; }

	const D3D12_SHADER_BYTECODE* GetShader(ResourceHandle handle) const { return &m_shaders[handle]; }
	MeshDX12& GetMesh(ResourceHandle handle)                            { return m_meshes[handle];   }

private:
	std::vector<MeshDX12> m_meshes;
	std::vector<D3D12_SHADER_BYTECODE> m_shaders;
	ID3D12Device* m_pDevice;
};