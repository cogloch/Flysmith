#pragma once
#include "Resources\Resource.h"
#include "StateObjects\RasterizerStateConfig.h"
#include "RenderItem.h"


class DescriptorHeap;
class ResourceCache;
class PSOManager;

// TODO: Handle state changes to render items outside -> make it a POD 
struct RenderItemDX12 : RenderItem
{
	RenderItemDX12();
	
	RenderItemDX12(RenderItemDX12&&);
	RenderItemDX12& operator=(RenderItemDX12&&);
	
	RenderItemDX12(const RenderItemDX12&) = delete;
	RenderItemDX12& operator=(const RenderItemDX12&) = delete;

	void Init(ResourceHandle mesh_, ResourceHandle vertShader, ResourceHandle pixelShader, ID3D12Device* pDevice, D3D12_CPU_DESCRIPTOR_HANDLE descHeapSlot, PSOManager*, FillMode = FillMode::SOLID, CullMode = CullMode::BACK);
	void UpdateTransform(const Matrix&);

	Matrix transform;
	ResourceHandle mesh;

	Resource worldMatConstBuffer;
	U8*      pWorldMatDataBegin;

	U32 psoId;
};