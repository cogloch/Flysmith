#include "PCH.h"
#include "RenderItemDX12.h"
#include "Descriptors\ConstantBufferView.h"
#include "Pipeline\PSOManager.h"
using namespace DirectX;


RenderItemDX12::RenderItemDX12(RenderItemDX12&& other)
{
	transform = other.transform;
	mesh = other.mesh;
	pWorldMatDataBegin = other.pWorldMatDataBegin;

	other.worldMatConstBuffer.Get()->AddRef();
	worldMatConstBuffer = other.worldMatConstBuffer;
}

RenderItemDX12& RenderItemDX12::operator=(RenderItemDX12&& other)
{
	transform = other.transform;
	mesh = other.mesh;
	pWorldMatDataBegin = other.pWorldMatDataBegin;

	other.worldMatConstBuffer.Get()->AddRef();
	worldMatConstBuffer = other.worldMatConstBuffer;

	return *this;
}

RenderItemDX12::RenderItemDX12()
	: mesh(0)
	, pWorldMatDataBegin(nullptr)
{
}

void RenderItemDX12::Init(ResourceHandle mesh_, ResourceHandle vertShader, ResourceHandle pixelShader, ID3D12Device* pDevice, D3D12_CPU_DESCRIPTOR_HANDLE descHeapSlot, PSOManager* psoManager, FillMode fillMode, CullMode cullMode)
{
	mesh = mesh_;

	// TODO: Consider caching
	ResourceConfig descCB(ResourceType::BUFFER, sizeof(XMFLOAT4X4));
	worldMatConstBuffer.CreateCommited(pDevice, descCB, &pWorldMatDataBegin);

	// TODO: Free function
	ConstantBufferView cbView(pDevice, worldMatConstBuffer.GetGPUVirtualAddress(), sizeof(XMFLOAT4X4), descHeapSlot);

	psoId = psoManager->GetPSOForConfiguration(vertShader, pixelShader, fillMode, cullMode);
}

void RenderItemDX12::UpdateTransform(const Matrix& newTransform)
{
	transform = newTransform;
	memcpy(pWorldMatDataBegin, &transform.Transpose(), sizeof(Matrix));
}