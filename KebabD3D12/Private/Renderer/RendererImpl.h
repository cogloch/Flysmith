#pragma once
#include "Renderer.h"

#include "Misc\HardwareCaps.h"
#include "RenderItemDX12.h"
#include "Camera.h"
#include "Device.h"
#include "Fence.h"

#include "Resources\ResourceCache.h"
#include "Resources\UploadHeap.h"

#include "Pipeline\ScissorRectangle.h"
#include "Pipeline\SwapChain.h"
#include "Pipeline\Viewport.h"
#include "Pipeline\CommandAllocator.h"
#include "Pipeline\CommandQueue.h"
#include "Pipeline\PSOManager.h"
#include "Pipeline\RootSignature.h"


struct IRenderer::Impl
{
	// Device
	Viewport m_viewport;
	ScissorRectangle m_scissorRect;
	HardwareCaps m_hwCaps;
	SwapChain m_swapChain;
	Device m_device;

	// Pipeline
	RootSignature    m_rootSignature;
	CommandAllocator m_commandAllocator;
	CommandQueue     m_commandQueue;
	CommandList		 m_commandList;

	// Resources
	UploadHeap m_uploadHeap;

	DescriptorHeap m_dsvDescHeap;
	Resource m_depthBuffer;

	DescriptorHeap m_cbDescHeap;
	Resource m_viewProjConstBuffer;
	U8* m_pViewProjDataBegin;
	
	PSOManager m_psoManager;
	Camera m_camera;

	// Synchronization
	Fence m_fence;
	U64 m_currentFence;
	HANDLE m_handleEvent;

	Impl(HWND hwnd, U32 windowWidth, U32 windowHeight, IRenderer*);

	void CreateRootSignature();
	void WaitForGPU();

	void PopulateCommandLists();
	void ExecuteCommandLists();
	void Present();

	ResourceCache m_resCache;
	U32 m_rootDescViewProjIndex;
	U32 m_rootDescTableIndex;
	// TODO: Temp
	const F32 m_clearColor[4] = { 90.0f / 255.0f, 136.0f / 255.0f, 255.0f / 255.0f, 1.0f };

	// Holds all render items, whether they are to be rendered or not.
	RenderItemDX12 m_renderItems[MAX_RENDER_ITEMS];

	IRenderer* m_pRenderer;
};