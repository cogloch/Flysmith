#include "PCH.h"
#include "Renderer.h"
#include "RendererImpl.h"
#include "Transform.h"

#pragma comment(lib, "D3d12.lib")
#pragma comment(lib, "DXGI.lib")
#pragma comment(lib, "d3dcompiler.lib")

#include "PerspectiveMatrix.h"


Renderer::Renderer(const HWND hwnd, const U32 windowWidth, const U32 windowHeight)
{
	m_pImpl = new Impl(hwnd, windowWidth, windowHeight, this);
}

Renderer::~Renderer()
{
	if (m_pImpl)
	{
		delete m_pImpl;
		m_pImpl = nullptr;
	}
}

// Copy visible render components
void Renderer::UpdateScene(std::vector<RenderItemProxy> renderables)
{
	for (size_t i = 0; i < m_numRenderItemCacheRequests; ++i)
	{
		auto& request = m_renderItemCacheQueue[i];
		m_pImpl->m_renderItems[m_numRenderItems++].Init(request.mesh, 
														request.vertShader, 
														request.pixelShader, 
														m_pImpl->m_device.Get(), 
														m_pImpl->m_cbDescHeap.GetCPUHandle(m_numRenderItems - 1),
														&m_pImpl->m_psoManager,
														FillMode::SOLID, CullMode::NONE);
	}
	m_numRenderItemCacheRequests = 0;
	
	m_renderQueueEnd = 0;
	for (auto& renderable : renderables)
	{
		auto itemHandle = renderable.renderItemId;
		m_pImpl->m_renderItems[itemHandle].UpdateTransform(renderable.transform);
		m_renderQueue[m_renderQueueEnd++] = itemHandle;
	}
}

// Copy camera state
void Renderer::UpdateView(const TransformNoScale& transform) const
{
	m_pImpl->m_camera.Update(transform);

	// TODO: cache
	PerspectiveMatrix perspectiveMat(800.0f, 600.0f, 0.8f, 1.0f, 1000.0f);

	Matrix wvp = m_pImpl->m_camera.GetViewProjMatrix(perspectiveMat).Transpose();
	memcpy(m_pImpl->m_pViewProjDataBegin, &wvp, sizeof(Matrix));
}

void Renderer::Render() const
{
	m_pImpl->PopulateCommandLists();
	m_pImpl->ExecuteCommandLists();
	m_pImpl->Present();
	m_pImpl->WaitForGPU();
}
	
ResourceHandle Renderer::CacheMesh(const VertexVec& verts, const IndexVec& indices) const
{
	return m_pImpl->m_resCache.AddMesh(verts, indices);
}

void Renderer::UpdateMesh(ResourceHandle handle, const VertexVec& verts, const IndexVec& indices) const
{
	m_pImpl->m_resCache.UpdateMesh(handle, verts, indices);
}

ResourceHandle Renderer::CacheShader(ShaderType type, const std::wstring& fullPath) const
{
	return m_pImpl->m_resCache.AddShader(type, fullPath.c_str());
}