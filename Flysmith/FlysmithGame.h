#pragma once
#include "Application.h"
#include "CameraController.h"
#include "Events\EventListener.h"
#include "Resources\ResourceLoader.h"
#include "Resources\ResourceRegistry.h"

// TEMP
#include "../../KebabGL/Public/RendererUI.h"


class FlysmithGame : public Application, public EventListener
{
public:
	explicit FlysmithGame(HINSTANCE);
	void HandleEvent(const Event&) override;

protected:
	// dt is in seconds
	void UpdateScene(float dt) override;

private:
	void LoadResources();
	CameraController m_camController;
	ResourceRegistry m_resources;
	ResourceLoader m_resourceLoader;

	// TEMP
	RendererUI* m_pUiRenderer;
};