#include "PCH.h"
#include "FlysmithGame.h"

#include "ShaderTypes.h"
#include "Aircraft\PlaneFactory.h"

#include "Events\KeyboardEvents.h"
#include "Physics\PhysicsSystem.h"


FlysmithGame::FlysmithGame(HINSTANCE hInstance)
	: Application(hInstance)
	, m_camController(&m_scene.camTransform)
{
	// TEMP
	m_pUiRenderer = &dynamic_cast<Renderer*>(m_pRenderer)->uiRenderer;

	RegisterForEvent("LMouseDown"_HASH);
	LoadResources();

	PlaneFactory::Create(m_resources, m_pRenderer, m_scene, "Cessna172S", m_pPhysicsSystem);
/*
	forceRegistry.AddForceGenerator(&m_scene.physicsComponents[0], &gravityGen);
	forceRegistry.AddForceGenerator(&m_scene.physicsComponents[0], &dragGen);*/
}

void FlysmithGame::HandleEvent(const Event& ev)
{
	switch (ev.type)
	{
	}
}

void FlysmithGame::LoadResources()
{
	m_resourceLoader.Init(m_pRenderer, &m_resources);

#ifdef RENDERER_DX12
	m_resourceLoader.Queue(SHADER, L"TestVS.hlsl", "TestVS", VERTEX_SHADER);
	m_resourceLoader.Queue(SHADER, L"TestPS.hlsl", "TestPS", PIXEL_SHADER);
#elif defined(RENDERER_GL)
	m_resourceLoader.Queue(SHADER, L"TestVS.glsl", "TestVS", VERTEX_SHADER);
	m_resourceLoader.Queue(SHADER, L"TestPS.glsl", "TestPS", PIXEL_SHADER);

	m_resourceLoader.Queue(SHADER, L"SpriteVS.glsl", "SpriteVS", VERTEX_SHADER);
	m_resourceLoader.Queue(SHADER, L"SpritePS.glsl", "SpritePS", PIXEL_SHADER);

	m_resourceLoader.Queue(SHADER, L"CookTorranceVS.glsl", "CookTorranceVS", VERTEX_SHADER);
	m_resourceLoader.Queue(SHADER, L"CookTorrancePS.glsl", "CookTorrancePS", PIXEL_SHADER);
#endif

	m_resourceLoader.Queue(FONT, L"cour.ttf", "AllerRegular");
}

void FlysmithGame::UpdateScene(float dt)
{
	m_pPhysicsSystem->Tick(dt);

	/*m_pUiRenderer->RenderLabel("Position: " + std::to_string(xform->GetPosition().x) + " " + std::to_string(xform->GetPosition().y) + " " + std::to_string(xform->GetPosition().z), m_resources.GetHandle("AllerRegular"), { -390, 250 }, { 1.0, 1.0, 1.0 });
	m_pUiRenderer->RenderLabel("Velocity: " + std::to_string(obj.velocity.x) + " " + std::to_string(obj.velocity.y) + " " + std::to_string(obj.velocity.z), m_resources.GetHandle("AllerRegular"), { -390, 220 }, { 1.0, 1.0, 1.0 });
	m_pUiRenderer->RenderLabel("Acceleration: " + std::to_string(obj.acceleration.x) + " " + std::to_string(obj.acceleration.y) + " " + std::to_string(obj.acceleration.z), m_resources.GetHandle("AllerRegular"), { -390, 190 }, { 1.0, 1.0, 1.0 });*/
}