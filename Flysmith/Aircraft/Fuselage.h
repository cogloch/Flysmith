#pragma once
#include "IAircraftElement.h"


struct FuselageRing
{
	float diameter;

	// Displacement from the aircraft's front
	float x;
	float y;
};

struct Fuselage : public IAircraftElement
{
	void ReadFromFile(const std::string& path) override;
	Mesh GenerateMesh() override;
	
	// TODO: Change to displacement from center
	std::vector<Vertex> GenerateCircularRing(F32 diameter, XMFLOAT3 dispFromFront) const;

	std::vector<FuselageRing> rings;
};