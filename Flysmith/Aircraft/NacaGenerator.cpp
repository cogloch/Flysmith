#include "PCH.h"
#include "NacaGenerator.h"


void NACA4Digit::Set(U32 type)
{
	maxCamber = type / 1000;
	maxCamberPos = type / 100 % 10;
	thickness = type % 100;
}

void NACA4Digit::Set(const std::string& type)
{
	Set(std::stoi(type));
}

void NACA4Digit::Set(U32 m, U32 p, U32 xx)
{
	maxCamber = m;
	maxCamberPos = p;
	thickness = xx;
}

// TEMP 
// Keep around for debugging; will do everything directly 
struct Sample
{
	float x;
	float yc, yt;
	float dycdx; // dyc 
				 // ---
				 // dx
	float theta;
	Vector2 upper;
	Vector2 lower;

	inline void Compute(float m, float p, float xx)
	{
		// Camber and gradient
		float xsq = x * x,
			  psq = p * p,
			  oneMinP = 1 - p,
			  oneMinPSq = oneMinP * oneMinP;

		if (x < p)  // Front section
		{
			yc = (m / psq) * (2 * p * x - xsq);
			dycdx = (2 * m / psq) * (p - x);
		}
		else        // Back section
		{
			yc = (m / oneMinPSq) * (1 - (2 * p) * (1 - x) - xsq);
			dycdx = (2 * m / oneMinPSq * (p - x));
		}

		theta = atanf(dycdx);

		// Thickness distribution
		static const float a0 = 0.2969f,
						   a1 = -0.126f,
						   a2 = -0.3516f,
						   a3 = 0.2843f,
						   a4 = -0.1015f; // Open trailing edge
						// a4 = -0.1036f; // Closed trailing edge

		float k0 = a0 * sqrtf(x),
			  k1 = a1 * x,
			  k2 = a2 * x * x,
			  k3 = a3 * x * x * x,
			  k4 = a4 * x * x * x * x;

		yt = 5 * xx * (k0 + k1 + k2 + k3 + k4);

		// Upper surface
		upper.x = x - yt * sinf(theta);
		upper.y = yc + yt * cosf(theta);

		// Lower surface
		lower.x = x + yt * sinf(theta);
		lower.y = yc - yt * cosf(theta);
	} 
};

std::array<std::vector<Vector2>, 2> NACA4Digit::Generate(U32 numSamples)
{
	enum
	{
		UPPER_SURFACE,
		LOWER_SURFACE
	};

	std::array<std::vector<Vector2>, 2> result;
	result[UPPER_SURFACE].reserve(numSamples);
	result[LOWER_SURFACE].reserve(numSamples);

	// Percentages
	float m = static_cast<float>(maxCamber) / 100.0f,
		  p = static_cast<float>(maxCamberPos) / 10.0f,
		  xx = static_cast<float>(thickness) / 100.0f;

	auto samples = new Sample[numSamples];

	samples[0].x = 0.0f;
	samples[0].Compute(m, p, xx);
	result[UPPER_SURFACE].push_back(samples[0].upper);
	result[LOWER_SURFACE].push_back(samples[0].lower);
	
	auto increment = 1.0f / static_cast<float>(numSamples);
	for (U32 i = 1; i < numSamples; ++i)
	{
		samples[i].x = samples[i - 1].x + increment;
		samples[i].Compute(m, p, xx);

		result[UPPER_SURFACE].push_back(samples[i].upper);
		result[LOWER_SURFACE].push_back(samples[i].lower);
	}

	delete[] samples;
	return result;
}

std::vector<Vector2> NACA4Digit::MergeUpperLowerVec(std::array<std::vector<Vector2>, 2>& surfaces)
{
	enum
	{
		UPPER_SURFACE,
		LOWER_SURFACE
	};

	std::vector<Vector2> merged;
	merged.reserve(surfaces[UPPER_SURFACE].size() + surfaces[LOWER_SURFACE].size());

	for (auto rit = surfaces[UPPER_SURFACE].rbegin(), rend = surfaces[UPPER_SURFACE].rend(); rit != rend; ++rit)
	{
		merged.push_back(*rit);
	}
	for (auto it = surfaces[LOWER_SURFACE].begin(), end = surfaces[LOWER_SURFACE].end(); it != end; ++it)
	{
		merged.push_back(*it);
	}

	return merged;
}
