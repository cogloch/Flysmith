#pragma once

class ResourceRegistry;
class PhysicsSystem;
class IRenderer;
class Transform;
class Scene;

using EntityId = I32; // TEMP

namespace PlaneFactory
{
	void CreateElement(Scene& scene, ResourceRegistry& resources, IRenderer* pRenderer, EntityId parentId, Transform& transform, const std::string& desc, const std::string& path);
	void Create(ResourceRegistry& resources, IRenderer* pRenderer, Scene& scene, const std::string& configName, PhysicsSystem*);
}