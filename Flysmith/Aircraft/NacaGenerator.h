#pragma once
#include "Vector.h"


struct NACA4Digit
{
	NACA4Digit() {}
	NACA4Digit(U32 type) { Set(type); }
	NACA4Digit(const std::string& type) { Set(type); }

	// MPXX format(max camber, max camber pos, thickness)
	void Set(U32 type);

	// MPXX format(max camber, max camber pos, thickness)
	void Set(const std::string& type);

	// m  - max camber
	// p  - position of the max camber from the leading edge 
	// xx - thickness
	void Set(U32 m, U32 p, U32 xx);

	// Same number of samples for both the upper&lower surfaces
	// First vector is the upper surface
	// Second vector is the lower surface
	std::array<std::vector<Vector2>, 2> Generate(U32 numSamples);

	// Clockwise upper surface TE -> LE then lower surface LE->TE
	static std::vector<Vector2> MergeUpperLowerVec(std::array<std::vector<Vector2>, 2>&);

	U32 maxCamber;    // m
	U32 maxCamberPos; // p
	U32 thickness;    // xx
};