#pragma once
#include "IAircraftElement.h"


class Wingtip : public IAircraftElement
{
public:
	enum Type
	{
		CUTOFF,
		ROUNDED,
		SHARP,
		HOERNER,
		DROOPED,
		ENDPLATE,
		WINGLET
	} type;

	static const std::map<std::string, Type> s_typeNames;

	// For now, it has to match the wing's airfoil at the tip.
	VertexVec tipAirfoil;
	
public:
	Wingtip();
	Mesh GenerateMesh() override;
	void ReadFromFile(const std::string& path) override;

private:
	void GenerateCutoff(Mesh& meshOut) const;
};