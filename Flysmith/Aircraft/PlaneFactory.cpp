#include "PCH.h"
#include "PlaneFactory.h"
#include "Resources\ResourceRegistry.h"
#include "Resources\AssetLocator.h"
#include "EntitySystem\Scene.h"
#include "IRenderer.h"
#include "Fuselage.h"
#include "Wing.h"
#include "json.hpp"
#include "Math\AngleMath.h"
#include "FileReader.h"
#include "Physics\PhysicsSystem.h"


void PlaneFactory::CreateElement(Scene& scene, ResourceRegistry& resources, IRenderer* pRenderer, EntityId parentId, Transform& transform, const std::string& desc, const std::string& path)
{
	// Create dummy entity
	auto elemId = scene.CreateEntity();

	// Set initial transform(relative to the master plane dummy)
	scene.entities[elemId].SetTransform(transform);

	IAircraftElement* pElement = nullptr;

	// Generate mesh for element
	// TODO: Currently mesh generation is complete shit. Should make the procedure data/script driven or 
	//       have a generic mesh generator with a set of rules(sort of like turtle graphics).	
	if      (desc == "Fuselage")						{ pElement = new Fuselage; }
	else if (desc == "RightWing" || desc == "LeftWing") { pElement = new Wing;     }
	
	if (!resources.ResourceExists(path))
	{
		pElement->ReadFromFile(path);
		auto mesh = pElement->GenerateMesh();
		resources.AddResource(path, pRenderer->CacheMesh(mesh.verts, mesh.indices));
	}

	// Create render component
	auto renderCompProxy = scene.CreateRenderComponent(resources.GetHandle(path), resources.GetHandle("TestVS"), resources.GetHandle("TestPS"));
	scene.AttachComponent(elemId, renderCompProxy);

	// Create physics component

	// Attach the fuselage entity to the dummy master plane entity 
	scene.entities[parentId].AddChild(&scene.entities[elemId]);

	delete pElement; // TODO: Keep track of the element. Maybe.
}

void PlaneFactory::Create(ResourceRegistry& resources, IRenderer* pRenderer, Scene& scene, const std::string& configName, PhysicsSystem* pPhysics)
{
	auto planeEntityId = scene.CreateEntity();

	std::string data;
	AssetLocator assLocator;
	FileReader::ReadSync(assLocator.GetAssetDirectoryA(AssetType::AIRCRAFT_CONFIGS) + configName + ".json", data);
	auto config = nlohmann::json::parse(data);

	auto elemArray = config["elements"];
	for (size_t idx = 0, numElem = elemArray.size(); idx < numElem; ++idx)
	{
		auto elem = elemArray[idx];

		Transform transform;
		auto pos = elem["position"];
		transform.SetPosition(pos["x"], pos["y"], pos["z"]);
		auto orientation = elem["orientation"];
		transform.SetRotation(DegToRad(orientation["x"]), DegToRad(orientation["y"]), DegToRad(orientation["z"]));

		CreateElement(scene, resources, pRenderer, planeEntityId, transform, elem["description"], elem["path"]);
	}

	auto physComponentProxy = scene.CreatePhysicsComponent();
	scene.AttachComponent(planeEntityId, physComponentProxy);
	auto physProps = pPhysics->GetPhysCompRef(physComponentProxy.index);
	physProps.inverseMass = 0.1f;
	physProps.velocity = { 0.0f };
	physProps.acceleration = { 0.0f };
}