#pragma once
#include "Mesh.h"


// All aircraft elements are treated equal when it comes to plane files:
// Each element has a GUID and aircraft config files are just lists of 
// aircraft element GUIDs along with positioning/fitting info.
class IAircraftElement
{
public:
	//U32 GetId() const { return m_id; }
	virtual void ReadFromFile(const std::string& path) = 0;
	virtual Mesh GenerateMesh() = 0;

private:
	//U32 m_id;
};

// Keeps track of aircraft element GUIDs with their associated file paths.
// May use hashed paths directly in the future.
//struct AircraftElementRegistry
//{
//};