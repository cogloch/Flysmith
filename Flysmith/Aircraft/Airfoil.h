#pragma once
#include "IAircraftElement.h"


class Airfoil : public IAircraftElement
{
public:
	Airfoil();
	Mesh GenerateMesh();
	void ReadFromFile(const std::string& path);
	std::vector<Vector2> points;
};