#pragma once


enum class AssetType
{
	NONE_ROOT,

	SHADERS,
	FONTS,

	AIRCRAFT_ELEMENTS,
	AIRCRAFT_CONFIGS,
	AIRFOILS,
	FUSELAGES,
	WINGS
};

class AssetLocator
{
public:
	AssetLocator();

	const std::wstring& GetAssetDirectory(AssetType, bool bTrailingBackslash = true) const;
	std::string GetAssetDirectoryA(AssetType, bool bTrailingBackslash = true) const;
	
	bool GetAssetPath(AssetType, const std::wstring& filename, std::wstring* outPath) const;
	bool GetAssetPathA(AssetType, const std::string& filename, std::string* outPath) const;

private:
	struct Impl;
	static std::unique_ptr<Impl> s_pImpl;
};