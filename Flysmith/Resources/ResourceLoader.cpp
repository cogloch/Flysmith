#include "PCH.h"
#include "ResourceRegistry.h"
#include "ResourceLoader.h"
#include "AssetLocator.h"
#include "Font.h"

#include <locale>
#include <codecvt>

#include "../KebabGL/Public/Renderer.h"


void ResourceLoader::Queue(ResourceType type, const std::wstring& filename, const std::string& name, U32 subType)
{
	// TEMP 
	// Will do the real multithreaded thing once shit actually works 
	
	static AssetLocator assLocator;

	switch (type)
	{
	case FONT:
	{
		static std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converterWtoA;
		auto filenameA = converterWtoA.to_bytes(filename);

		Font font;
		font.LoadFromFile((assLocator.GetAssetDirectoryA(AssetType::FONTS) + filenameA).c_str());
		m_pResourceRegistry->AddResource(name, dynamic_cast<Renderer*>(m_pRenderer)->CacheFont(font));
		break;
	}
	case SHADER:
	{
		m_pResourceRegistry->AddResource(name, m_pRenderer->CacheShader(static_cast<ShaderType>(subType), assLocator.GetAssetDirectory(AssetType::SHADERS) + filename));
		break;
	}
	}
}

void ResourceLoader::Init(IRenderer* pRenderer, ResourceRegistry* pResReg)
{
	assert(pRenderer);
	assert(pResReg);
	m_pRenderer = pRenderer;
	m_pResourceRegistry = pResReg;
}
