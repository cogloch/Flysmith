#include "PCH.h"
#include "AssetLocator.h"
#include "FileSystem.h"
#include <codecvt>
#include <locale>


struct AssetLocator::Impl
{
	Impl();

	std::map<AssetType, std::wstring> assetDirectories;
	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converterWtoA;
};
std::unique_ptr<AssetLocator::Impl> AssetLocator::s_pImpl = nullptr;

AssetLocator::AssetLocator()
{
	if (s_pImpl == nullptr)
	{
		s_pImpl = std::make_unique<Impl>();
	}
}

const std::wstring& AssetLocator::GetAssetDirectory(AssetType assetType, bool bTrailingBackslash) const
{
	if (!bTrailingBackslash)
	{
		return std::move(s_pImpl->assetDirectories[assetType].substr(0, s_pImpl->assetDirectories[assetType].size() - 1));
	}

	return s_pImpl->assetDirectories[assetType];
}

std::string AssetLocator::GetAssetDirectoryA(AssetType assetType, bool bTrailingBackslash) const
{
	return s_pImpl->converterWtoA.to_bytes(GetAssetDirectory(assetType, bTrailingBackslash));
}

bool AssetLocator::GetAssetPath(AssetType assetType, const std::wstring& filename, std::wstring* outPath) const
{
	*outPath = GetAssetDirectory(assetType) + filename;
	return FileSystem::FileExists(outPath->c_str());
}

bool AssetLocator::GetAssetPathA(AssetType assetType, const std::string & filename, std::string* outPath) const
{
	*outPath = GetAssetDirectoryA(assetType) + filename;
	return FileSystem::FileExistsA(outPath->c_str());
}

AssetLocator::Impl::Impl()
{
	FileSystem fs;
	std::wstring rootPath(fs.GetExePath());

#ifdef _DEBUG
	// Root is the Solution folder

	// Remove executable name
	FileSystem::RemoveLastNameFromPath(&rootPath, true);

	// Remove build configuration folder
	FileSystem::RemoveLastNameFromPath(&rootPath, true);

	// Remove Bin folder
	FileSystem::RemoveLastNameFromPath(&rootPath, true);
#else /* REDISTRIBUTING */
	// Root is the executable folder

	// Remove executable name and trailing slash
	rootPath = rootPath.substr(0, rootPath.find_last_of(L"\\") + 1);
#endif

	assetDirectories[AssetType::NONE_ROOT] = rootPath + L"Assets\\";

	assetDirectories[AssetType::SHADERS] = assetDirectories[AssetType::NONE_ROOT] + L"Shaders\\";
	assetDirectories[AssetType::FONTS] = assetDirectories[AssetType::NONE_ROOT] + L"Fonts\\";

	auto aircraftDir = assetDirectories[AssetType::NONE_ROOT] + L"Aircraft\\";
	assetDirectories[AssetType::AIRCRAFT_ELEMENTS] = aircraftDir + L"Elements\\";
	assetDirectories[AssetType::AIRCRAFT_CONFIGS] = aircraftDir + L"Configurations\\";

	assetDirectories[AssetType::AIRFOILS] = assetDirectories[AssetType::AIRCRAFT_ELEMENTS] + L"Airfoils\\";
	assetDirectories[AssetType::FUSELAGES] = assetDirectories[AssetType::AIRCRAFT_ELEMENTS] + L"Fuselages\\";
	assetDirectories[AssetType::WINGS] = assetDirectories[AssetType::AIRCRAFT_ELEMENTS] + L"Wings\\";
}