#pragma once


class IRenderer;
class ResourceRegistry;

enum ResourceType
{
	FONT,
	SHADER
};

class ResourceLoader
{
public:
	void Queue(ResourceType, const std::wstring& filename, const std::string& name, U32 subType = 0);
	void Init(IRenderer*, ResourceRegistry*);

private:
	IRenderer* m_pRenderer;
	ResourceRegistry* m_pResourceRegistry;
};