#pragma once


#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <algorithm>
#include <cassert>
#include <fstream>
#include <string>
#include <memory>
#include <vector>
#include <cstdio>
#include <array>
#include <map>

// TODO: Replace with api neutral math wrapper
#include <DirectXMath.h>

#include "Logger.h"
#include "Types.h"