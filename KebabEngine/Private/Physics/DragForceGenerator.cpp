#include "PCH.h"
#include "EntitySystem\PhysicsComponent.h"
#include "Physics\DragForceGenerator.h"
#include "Vector.h"


DragForceGenerator::DragForceGenerator(float coeffLin, float coeffQuad)
	: m_coeffLin(coeffLin)
	, m_coeffQuad(coeffQuad)
{
}

void DragForceGenerator::UpdateForce(PhysicsComponent* pPhysComp, float dt)
{
	// f = drag force
	// vdir = velocity direction
	// m_coeffLin = linear drag coefficient
	// m_coeffQuad = quadratic drag coefficient 
	//
	// f = -vel_dir * (m_coeffLin * speed + m_coeffQuad * speed^2)

	Vector3 force = pPhysComp->velocity;
	
	float totalDragCoeff = force.GetMagnitude();
	totalDragCoeff = m_coeffLin * totalDragCoeff + 
					 m_coeffLin * totalDragCoeff * totalDragCoeff;
	
	force.Normalize();
	// Acting in opposite direction
	force *= -totalDragCoeff;

	pPhysComp->AddForce(force);
}