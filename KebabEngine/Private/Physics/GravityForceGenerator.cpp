#include "PCH.h"
#include "Physics\GravityForceGenerator.h"
#include "EntitySystem\PhysicsComponent.h"


GravityForceGenerator::GravityForceGenerator(const Vector3& gravityAcc)
	: m_gravityAcc(gravityAcc)
{
}

void GravityForceGenerator::UpdateForce(PhysicsComponent* pPhysComp, float)
{
	if (!pPhysComp->GetMass()) return;

	pPhysComp->AddForce(m_gravityAcc * pPhysComp->GetMass());
}