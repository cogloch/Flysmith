#include "PCH.h"
#include "Physics\PhysicsSystem.h"
#include "Physics\Integrators.h"
#include "EntitySystem\Scene.h"


PhysicsSystem::PhysicsSystem()
	: gravityGen({ 0.0f, -9.8f, 0.0f })
	, dragGen(200.f, 1.f)
{
}

void PhysicsSystem::Tick(float dt)
{
	forceRegistry.UpdateForces(dt);

	for (auto& obj : physicsComponents)
	{
		auto xform = pScene->entities[obj.GetEntityId()].GetTransform();
		Integrators::IntegrateEuler(obj, *xform, dt);
	}
}

PhysicsComponent& PhysicsSystem::GetPhysCompRef(U32 id)
{
	return physicsComponents[id];
}
