#include "PCH.h"
#include "EntitySystem\RenderComponent.h"
#include "IRenderer.h"


RenderComponent::RenderComponent(IRenderer* pRenderer, U32 mesh, U32 vertShader, U32 pixelShader)
{
	m_renderItem = pRenderer->AddRenderItem(mesh, vertShader, pixelShader);
}