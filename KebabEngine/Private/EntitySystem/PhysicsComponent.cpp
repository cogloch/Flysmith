#include "PCH.h"
#include "EntitySystem\PhysicsComponent.h"


PhysicsComponent::PhysicsComponent() : inverseMass(0.0f)
{
}

void PhysicsComponent::SetMass(float mass)
{
	assert(mass > 0.0f);
	inverseMass = 1.0f / mass;
}

void PhysicsComponent::AddForce(const Vector3& force)
{
	netForce += force;
}

void PhysicsComponent::ClearAccumulator()
{
	netForce = { 0 };
}

float PhysicsComponent::GetMass() const
{
	assert(inverseMass > 0.0f);
	return 1 / inverseMass;
}
