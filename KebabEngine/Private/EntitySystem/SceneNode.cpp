#include "PCH.h"
#include "EntitySystem\SceneGraph.h"
#include "EntitySystem\SceneNode.h"


Matrix SceneNode::GetWorldTransform() const
{
	Matrix world;
	world.SetFromXM(GetWorldTransformXM());
	return world;
}

XMMATRIX SceneNode::GetWorldTransformXM() const 
{
	if (parent == -1)
	{
		return transform.GetMatrixXM();
	}

	return transform.GetMatrixXM() * pSceneGraph->nodes[parent].GetWorldTransformXM();
}