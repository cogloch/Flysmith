#pragma once
#include "EntitySystem/PhysicsComponent.h"
#include "Transform.h"


namespace Integrators
{
	inline void KEBAB_API IntegrateEuler(PhysicsComponent& obj, Transform& xform, float dt)
	{
		// Ignore things with infinite mass
		if (obj.inverseMass <= 0.0f) return;

		// Acceleration from net force
		obj.acceleration = obj.netForce * obj.inverseMass;

		// Single term expansion for velocity
		// v(t + dt) = v(t) + dt*(dv/dt)
		auto newVel = obj.velocity + dt * obj.acceleration;

		// Single term expansion for position
		// s(t + dt) = s(t) + dt*(ds/dt)
		auto newPos = xform.GetPositionXM() + dt * newVel.GetXMVec();

		xform.SetPosition(newPos);
		obj.velocity = newVel;

		// Clear forces
		obj.ClearAccumulator();
	}

	inline void KEBAB_API IntegrateRK(PhysicsComponent& obj, Transform& xform, float dt)
	{
	}
};