#pragma once
#include "EntitySystem\PhysicsComponent.h"
#include "GravityForceGenerator.h"
#include "DragForceGenerator.h"


class KEBAB_API PhysicsSystem
{
public:
	PhysicsSystem();
	void Tick(float dt); // May do updating of the physics system with constant timesteps.
	
	// Used to update physical properties of the component with the passed in @id.
	PhysicsComponent& GetPhysCompRef(U32 id);

private:
	friend class Scene;
	Scene* pScene;
	std::vector<PhysicsComponent> physicsComponents;

	ForceRegistry forceRegistry;
	GravityForceGenerator gravityGen;
	DragForceGenerator dragGen;
};