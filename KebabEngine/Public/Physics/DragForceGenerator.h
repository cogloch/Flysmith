#pragma once
#include "Forces.h"
#include "PublicDef.h"


class KEBAB_API DragForceGenerator final : public IForceGenerator
{
public:
	DragForceGenerator(float coeffLin, float coeffQuad);
	void UpdateForce(PhysicsComponent*, float dt) override;

private:
	float m_coeffLin;
	float m_coeffQuad;
};

