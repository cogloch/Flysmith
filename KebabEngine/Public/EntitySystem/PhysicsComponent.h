#pragma once
#include "Component.h"
#include "Vector.h"


class KEBAB_API PhysicsComponent : public Component
{
public:
	PhysicsComponent();

	Type GetType() const override { return PHYSICS; }

	void AddForce(const Vector3&);
	void ClearAccumulator();

	void SetMass(float);
	float GetMass() const;
	bool HasFiniteMass() const { return inverseMass != 0.0f; }
	
	// Net force acting on object
	Vector3 netForce;

	// 0 inverse mass is equivalent to infinite mass 
	// while infinite inverse mass(mass = 0) is impossible
	// Done to avoid division by 0 during integration(and also avoid the additional division each step)
	float inverseMass;
	
	Vector3 velocity;
	Vector3 acceleration;
};