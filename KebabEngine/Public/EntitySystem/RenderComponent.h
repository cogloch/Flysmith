#pragma once
#include "Component.h"


class IRenderer;

class KEBAB_API RenderComponent : public Component
{
public:
	RenderComponent(IRenderer*, U32 mesh, U32 vertShader, U32 pixelShader);
	
	U32 GetRenderItem() const     { return m_renderItem; }
	Type GetType() const override { return RENDER;       }

private:
	U32 m_renderItem;
};