#pragma once
#include "PublicDef.h"


class KEBAB_API Component
{
public:
	enum Type
	{
		RENDER,
		PHYSICS,

		NUM_TYPES
	};

public:
	void AttachToEntity(I32 entityId) { m_entityId = entityId; }
	I32 GetEntityId() const           { return m_entityId;     }
	
	virtual ~Component() = default;
	virtual Type GetType() const = 0;

protected:
	Component() : m_entityId(-1) {}
	I32 m_entityId;
};