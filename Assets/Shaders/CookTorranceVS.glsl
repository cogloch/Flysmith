#version 450 core

// Per-Vertex Input
layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec3 vertexNormal;

// Per-Object Input
uniform mat4 modelMatrix;
uniform mat4 viewProjMatrix;

// Vertex Shader Output
out vec3 Normal;
out vec3 FragPos;

void main()
{
	FragPos = vec3(modelMatrix * vec4(vertexPos, 1.0f));
	Normal  = mat3(transpose(inverse(modelMatrix))) * vertexNormal;
	
	gl_Position = viewProjMatrix * modelMatrix * vec4(vertexPos, 1.0f);
}