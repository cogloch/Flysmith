#version 450 core

layout (location = 0) in vec2 pos;
uniform mat4 proj;

void main()
{
	mat4 scale = mat4( 20, 0, 0, 0,
	                   0, 20, 0, 0, 
				       0, 0, 20, 0, 
				       0, 0, 0, 1 );
	gl_Position = proj * scale * vec4(pos, 0.0, 1.0);
}