#version 450 core

layout (location = 0) in vec3 vertexPos;
uniform mat4 viewProjMatrix;

void main()
{
	mat4 model = mat4(1, 0, 0, 0,
					  0, 1, 0, 0,
					  0, 0, 1, 0, 
					  0, 0, 0, 1);
	gl_Position = viewProjMatrix *  model * vec4(vertexPos, 1.0f);
} 

