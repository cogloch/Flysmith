#version 450 core

in vec2 texCoord;
out vec4 fragColor;

uniform sampler2D text;
uniform vec3 color;

void main()
{    
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, texCoord).r);
    fragColor = vec4(color, 1.0) * sampled;
}  