#version 450 core

// Pixel Shader Output
out vec4 color;

// Vertex Shader Output
in vec3 FragPos;
in vec3 Normal;

// Scene Constants
uniform vec3 lightPos;
uniform vec3 viewPos;

// Material
uniform float roughness;
uniform float f0;        // Reflectance at normal incidence
uniform vec3 specular;
uniform vec3 diffuse;

void main()
{
	vec3 halfVector = normalize(lightPos + viewPos);
	
	// Geometric Term
	float geoNumerator   = 2.0f * clamp(dot(Normal, halfVector), 0.0f, 1.0f);  // 2 * (Normal dot Half)
	float geoDenominator = clamp(dot(viewPos, halfVector), 0.0f, 1.0f);        // View dot Half
	
	float geoTerm = min(1.0f, min(geoNumerator * clamp(dot(Normal, viewPos), 0.0f, 1.0f) / geoDenominator,
	                              geoNumerator * clamp(dot(Normal, lightPos), 0.0f, 1.0f) / geoDenominator)
						);
						
	// Roughness Term
	// Beckmann Distribution
	float distribExpNumerator = pow(clamp(dot(Normal, halfVector), 0.0f, 1.0f), 2) - 1.0f;                      // Normal dot Half squared - 1
	float distribExpDenominator = pow(roughness, 2) * pow(clamp(dot(Normal, halfVector), 0.0f, 1.0f), 2);       // roughness squared * Normal dot Half squared
	float distribNumerator = exp(distribExpNumerator / distribExpDenominator);                                  // e ^ ((Normal dot Half squared - 1) / (roughness squared * Normal dot Half squared))
	float distribDenominator = 3.1415 * pow(roughness, 2) * pow(clamp(dot(Normal, halfVector), 0.0f, 1.0f), 4); // PI * roughness squared * (Normal dot Half)^4
	float distribTerm = distribNumerator / distribDenominator;
	
	// Fresnel Term
	float fresnelTerm = f0 + pow(1.0f - clamp(dot(viewPos, halfVector), 0.0f, 1.0f), 5.0f) * (1.0f - f0);
	
	// Specular Term
	float RsNumerator = (fresnelTerm * geoTerm * distribTerm);
	float RsDenominator = 3.1415 * clamp(dot(Normal, viewPos), 0.0f, 1.0f) * clamp(dot(Normal, lightPos), 0.0f, 1.0f);
	float Rs =  RsNumerator / RsDenominator;
	
	// Resulting Color 
	vec3 R = max(0.0f, clamp(dot(Normal, lightPos), 0.0f, 1.0f)) * (specular * Rs + diffuse);
	
	color = vec4(R, 1.0f);
}